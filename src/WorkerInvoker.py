#!/usr/bin/python

from Util import Util
import os
import sys

pwd = sys.stdin.readline()
pwd = pwd.split("\n")[0]

util = Util()
baseDir = util.get_base_dir()
host = util.get_host_name()
os.popen("echo %s | /usr/bin/python %s/src/Worker.py &> %s/log/%s.log &"%(pwd, baseDir, baseDir, host))
