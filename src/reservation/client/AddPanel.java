package dcapi.client;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

import java.sql.Timestamp;

import dcapi.app.*;
import dcapi.util.*;
import com.google.gson.Gson;

public class AddPanel extends JPanel{

    public static final String CHARSET = "abcdefghijklmnopqrstuvwxyz0123456789-_.";
    public static final String USER_NAME = "usr id:";
    public static final String USER_EMAIL = "email:";
    public static final String START_TIME = "start:";
    public static final String END_TIME = "end:";
    public static final String [] LABEL_NAMES = {USER_NAME, USER_EMAIL, START_TIME, END_TIME};
    public static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_PATTERN = "<HTML><BODY>Time Format: <br/>" + TIME_FORMAT + "</BODY></HTML>";

    public static final int ADD_PANEL_WIDTH = 300;
    public static final int ADD_PANEL_HEIGHT = 500;

    public static final int FORMAT_START_X = 20;
    public static final int FORMAT_START_Y = 20;
    public static final int FORMAT_WIDTH = 230;
    public static final int FORMAT_HEIGHT = 60;

    public static final int LABEL_SPACE = 20;
    public static final int LABEL_WIDTH = 80;
    public static final int LABEL_HEIGHT = 30;    
    public static final int LABEL_START_X = 20;
    public static final int LABEL_START_Y = FORMAT_START_Y + FORMAT_HEIGHT * 2 + LABEL_SPACE;

    public static final int TEXT_START_X = LABEL_START_X + LABEL_WIDTH;
    public static final int TEXT_START_Y = LABEL_START_Y;
    public static final int TEXT_WIDTH = 150;
    public static final int TEXT_HEIGHT = 30;
    public static final int TEXT_SPACE = 20;

    public static final int BUTTON_WIDTH = 90;
    public static final int BUTTON_HEIGHT = 30;
    public static final int BUTTON_SPACE = FORMAT_WIDTH - 2 * BUTTON_WIDTH;    

    public static final int CONFIRM_START_X = 20;
    public static final int CONFIRM_START_Y = TEXT_START_Y + LABEL_NAMES.length * (TEXT_SPACE + TEXT_HEIGHT);

    public static final int CANCEL_START_X = CONFIRM_START_X + BUTTON_WIDTH + BUTTON_SPACE;
    public static final int CANCEL_START_Y = CONFIRM_START_Y;

    private JLabel [] dataLabels = null;
    private JLabel formatLabel = null;

    private JTextField [] dataText = null;

    private JButton confirm = null;
    private JButton cancel = null;

    private CCNQuerySender ccnSender = null;

    private ConfKeeper conf = null;

    private JTextArea infoArea = null;

    public AddPanel(JTextArea infoArea){
        this(new ConfKeeper(ConfKeeper.SELECT_CCN_PREFIX | 
                            ConfKeeper.SELECT_BASE_DIR |
                            ConfKeeper.SELECT_INVALID_CHAR), infoArea);
    }

    public AddPanel(ConfKeeper conf, JTextArea infoArea){
        this.infoArea = infoArea;
        this.conf = conf;
        this.ccnSender = new CCNQuerySender(this.conf);
        initGUI();
    }

    private void initGUI(){
        this.setLayout(null);
        int i = 0;
        int dataFieldNum = LABEL_NAMES.length;

        //add notice labels
        formatLabel = new JLabel(FORMAT_PATTERN);

        formatLabel.setFont(new Font("Dialog", 0, 18));

        formatLabel.setBounds(FORMAT_START_X, FORMAT_START_Y, FORMAT_WIDTH, FORMAT_HEIGHT);
        
        this.add(formatLabel);
        
        //add data lables
        dataLabels = new JLabel[dataFieldNum];
        
        for( i = 0; i < dataFieldNum; ++i){
            dataLabels[i] = new JLabel(LABEL_NAMES[i]);
            dataLabels[i].setBounds(LABEL_START_X, LABEL_START_Y + (i - 1) * (LABEL_SPACE + LABEL_HEIGHT), LABEL_WIDTH, LABEL_HEIGHT);
            dataLabels[i].setFont(new Font("Dialog", 0, 18));
            this.add(dataLabels[i]);
        }

        //add text field
        dataText = new JTextField[dataFieldNum];
        
        for( i = 0 ; i < dataFieldNum; ++i){
            dataText[i] = new JTextField();
            dataText[i].setBounds(TEXT_START_X, TEXT_START_Y + (i - 1) * (TEXT_SPACE + TEXT_HEIGHT), TEXT_WIDTH, TEXT_HEIGHT);
            this.add(dataText[i]);
        }

        //add button
        confirm = new JButton("Confirm");
        cancel = new JButton("Cancel");

        confirm.setBounds(CONFIRM_START_X, CONFIRM_START_Y, BUTTON_WIDTH, BUTTON_HEIGHT);
        cancel.setBounds(CANCEL_START_X, CANCEL_START_Y, BUTTON_WIDTH, BUTTON_HEIGHT);

        confirm.addActionListener(new ConfirmListener());

        this.add(confirm);
        this.add(cancel);
        


    }

    public Reservation strToReserv(String [] name, String [] data){
        int len = name.length;
        Reservation reserv = new Reservation();
        try{
            for(int i = 0; i < len; ++i){
                if(name[i].compareTo(USER_NAME) == 0){
                    reserv.setUserName(data[i]);
                }
                else if(name[i].compareTo(USER_EMAIL) == 0){
                    reserv.setUserEmail(data[i]);
                }
                else if(name[i].compareTo(START_TIME) == 0){
                    reserv.setStartTime(Timestamp.valueOf(data[i]));
                }
                else if(name[i].compareTo(END_TIME) == 0){
                    reserv.setEndTime(Timestamp.valueOf(data[i]));
                }
                else{
                    JOptionPane.showMessageDialog(null, "Internal error: please update the software!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return null;
                }
            }
        }
        catch(IllegalArgumentException ex){
            JOptionPane.showMessageDialog(null, "Invalid time format! Please use " + TIME_FORMAT + ".", "ERROR", JOptionPane.ERROR_MESSAGE);
            return null;
        }
        int checkRes = Reservation.checkReservation(reserv);
        if(Reservation.VALID_RESERVATION != checkRes){
            JOptionPane.showMessageDialog(null, "Invalid Reservation! Error code : " + checkRes, "ERROR", JOptionPane.ERROR_MESSAGE);
            return null;
        }
        return reserv;

    }

    public Reservation getReserv(){
        int dataFieldNum = LABEL_NAMES.length;
        String data[] = new String[dataFieldNum];
        int i = 0 ;
        for(i = 0 ; i < dataFieldNum; ++i){
            data[i] = dataText[i].getText();
        }
        return strToReserv(LABEL_NAMES, data);
    }



    public int handleResponse(Response response){
        int code = response.getCode();
        switch(code){
            case Response.ADD_SUCCESS:
                JOptionPane.showMessageDialog(null, "Congratulations!" + 
                                                    "You have successfully made your reservation.\n" + 
                                                    "Please check your email for login user name and password.", 
                                                    "Information", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Reservation Accomplished!\n");
                break;
            case Response.GET_SUCCESS:
                JOptionPane.showMessageDialog(null, "Internal Error! Received GET_SUCCESS from ADD action!", "Error", JOptionPane.ERROR_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Internal Error! Received GET_SUCCESS from ADD action!\n"+ "\n");
                break;
            case Response.INVALID_CMD:
                JOptionPane.showMessageDialog(null, "Internal Error! Unsupported cmd is sent! Please update your software.", 
                                                    "Error", JOptionPane.ERROR_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Internal Error! Unsupported cmd is sent! Please update your software."+ "\n");
                break;
            case Response.INVALID_SLOT:
                JOptionPane.showMessageDialog(null, "Invalid Reservation!\n" + 
                                                    "Your reservation length has to be in range [1H, 24H]", 
                                                    "Invalid Reservation", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Your reservation length has to be in range [1H, 24H]"+ "\n");
                break;
            case Response.INVALID_START:
                JOptionPane.showMessageDialog(null, "Invalid Reservation!\n" +
                                                    "Your reservation has to start within 24 hours in the future." , 
                                                    "Invalid Reservation", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Your reservation has to start within 24 hours in the future."+ "\n");
                break;
            case Response.INVALID_USR:
                JOptionPane.showMessageDialog(null, "Invalid Username!\n" +
                                                    "Username has to match " + Reservation.userNamePattern, 
                                                    "Invalid Username", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Username has to match " + Reservation.userNamePattern+ "\n");
                break;
            case Response.INVALID_PWD:
                JOptionPane.showMessageDialog(null, "Internal Error! Received impossible response about password!", 
                                                    "ERROR", JOptionPane.ERROR_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Internal Error! Received impossible response about password!"+ "\n");
                break;
            case Response.INVALID_EMAIL:
                JOptionPane.showMessageDialog(null, "Invalid Email!", "Invalid Email", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + "Invalid Email"+ "\n");
                break;
            case Response.CONFLICT:
                JOptionPane.showMessageDialog(null, "Reservation Conflict!" + 
                                                    "Please click check button to see current reservations and find a available slot",
                                                    "Reservation Conflict", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + 
                        "Please click check button to see current reservations and find a available slot" + "\n");
                break;    
            case Response.CONFLICT_USR:
                JOptionPane.showMessageDialog(null, "Username Conflict!" + 
                                                    "Please choose another one.",
                                                    "Username Conflict", JOptionPane.INFORMATION_MESSAGE);
                infoArea.append(new Timestamp(System.currentTimeMillis()) + " username conflict");
                break;
        }
        return code;
    }

    public static void main(String args[]){
        JFrame jf = new JFrame();
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        AddPanel jp = new AddPanel(new JTextArea());
        jf.add(jp);
        jf.setSize(AddPanel.ADD_PANEL_WIDTH, ADD_PANEL_HEIGHT);
        jf.setVisible(true);

    }

    class ConfirmListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Reservation reserv = getReserv();
            Gson gson = new Gson();
            System.out.println(gson.toJson(reserv));
            if(null == reserv){
                return;
            }
            Request request = new Request(Request.CMD_ADD, reserv);
            Response response = ccnSender.sendQuery(request);
            if(null == response){
                JOptionPane.showMessageDialog(null, "Internal Error", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            handleResponse(response);
        } 
    }

    //TODO: implement cancel

}
