package dcapi.client;

import dcapi.util.*;
import dcapi.app.*;

import javax.swing.table.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

import java.sql.Timestamp;

public class CheckButtonListener implements ActionListener{
    
    private JTextArea infoArea = null;
    private DefaultTableModel tableModel = null;
    private CCNQuerySender ccnSender = null;

    public CheckButtonListener(JTextArea infoArea, DefaultTableModel tableModel){
        this.infoArea = infoArea;
        this.tableModel = tableModel;
        this.ccnSender = new CCNQuerySender();
    }    

    
    public void displayReservList(Response response){
        ReservationList reservList = response.getReservList();
        Reservation reserv = null;
        int len = reservList.size();
        String [][] data = new String[len][MainPanel.TABLE_COL_NUM];
        int i;        

        int rowCnt = tableModel.getRowCount();
        for(i = rowCnt -1 ; i >=0; i --){
            tableModel.removeRow(i);
        }
        
    
        for(i = 0 ; i < len; ++i){
            reserv = reservList.get(i);
            data[i][0] = String.valueOf(reserv.getReservID());
            data[i][1] = reserv.getUserName();
            data[i][2] = String.valueOf(reserv.getStartTime());
            data[i][3] = String.valueOf(reserv.getEndTime());
            tableModel.addRow(data[i]);
        }
    }

    public void actionPerformed(ActionEvent e){
        Request request = new Request(Request.CMD_GET, null);
        Response response = ccnSender.sendQuery(request);
        if(null == response){
            JOptionPane.showMessageDialog(null, "Cannot connect to Server!", "ERROR", JOptionPane.INFORMATION_MESSAGE);
            infoArea.append(new Timestamp(System.currentTimeMillis()) +  "Cannot connect to Server!\n");
            return;
        }
        displayReservList(response);
    }
}
