package dcapi.client;

import dcapi.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

public class AddButtonListener implements ActionListener{

    private JTextArea infoArea = null;

    public AddButtonListener(JTextArea infoArea){
        this.infoArea = infoArea;
    }

    public void actionPerformed(ActionEvent e){
        JFrame jf = new JFrame();
        AddPanel jp = new AddPanel(infoArea);
        jf.add(jp);
        jf.setSize(AddPanel.ADD_PANEL_WIDTH, AddPanel.ADD_PANEL_HEIGHT);
        jf.setResizable(false);
        jf.setVisible(true);
    }
}
