package dcapi.client;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

public class MainPanel extends JPanel{
    
    public static final int BORDER_HEIGHT = 60;

    public static final int MAIN_WIDTH = 600;
    public static final int MAIN_HEIGHT = 600; 
    
    public static final int TABLE_START_X = 20;
    public static final int TABLE_START_Y = 20;
    public static final int TABLE_WIDTH = 400;
    public static final int TABLE_HEIGHT = 530;

    public static final int TABLE_ROW_NUM = 35;
    public static final int TABLE_COL_NUM = 4;    

    private static final int TABLE_X_REMAIN = MAIN_WIDTH - TABLE_START_X - TABLE_WIDTH;

    public static final int INFO_WIDTH = 140;
    public static final int INFO_HEIGHT = 400;
    public static final int INFO_START_X = (int)(TABLE_START_X + TABLE_WIDTH + (TABLE_X_REMAIN - INFO_WIDTH) / 2);
    public static final int INFO_START_Y = 20;

    private static final int INFO_Y_REMAIN = MAIN_HEIGHT - BORDER_HEIGHT - INFO_HEIGHT;

    //TODO: add confirmation button
    public static final int BUTTON_NUM = 3;
    public static final int BUTTON_WIDTH = 80;
    public static final int BUTTON_HEIGHT = 30;
    public static final int BUTTON_Y_SPACE = (int)(INFO_Y_REMAIN - BUTTON_NUM * BUTTON_HEIGHT) / (BUTTON_NUM + 1);
    public static final int BUTTON_START_X = (int)(TABLE_START_X + TABLE_WIDTH + (TABLE_X_REMAIN - BUTTON_WIDTH) / 2);    

    public static final int ADD_INDEX = 1;
    public static final String ADD_NAME = "add";
    public static final int ADD_START_X = BUTTON_START_X;
    public static final int ADD_START_Y = INFO_START_Y + INFO_HEIGHT + BUTTON_Y_SPACE * ADD_INDEX + BUTTON_HEIGHT * (ADD_INDEX - 1);

    public static final int CANCEL_INDEX = 2;
    public static final String CANCEL_NAME = "cancel";
    public static final int CANCEL_START_X = BUTTON_START_X;
    public static final int CANCEL_START_Y = INFO_START_Y + INFO_HEIGHT + BUTTON_Y_SPACE * CANCEL_INDEX + BUTTON_HEIGHT * (CANCEL_INDEX - 1);

    public static final int CHECK_INDEX = 3;
    public static final String CHECK_NAME = "check";
    public static final int CHECK_START_X = BUTTON_START_X;
    public static final int CHECK_START_Y = INFO_START_Y + INFO_HEIGHT + BUTTON_Y_SPACE * CHECK_INDEX + BUTTON_HEIGHT * (CHECK_INDEX - 1);

    private static final String [] colNames = {"Reservation ID", "User", "Start", "End"};

    //reservation table
    private JScrollPane reservPanel = null;
    private JTable reservTable = null;
    private String [][] rowData = null;
    private DefaultTableModel tableModel = null;

    //information Area
    private JScrollPane infoPanel = null;
    private JTextArea infoArea = null;

    //buttons
    private JButton addButton = null;
    private JButton cancelButton = null;
    private JButton checkButton = null;
    

    public MainPanel(){
        initGUI();
    }

    private void initGUI(){
        
        this.setLayout(null);
        int i = 0;
        //init reservation table
        rowData = new String [TABLE_ROW_NUM][TABLE_COL_NUM];
        //reservTable = new JTable(rowData, colNames);
        reservTable = new JTable();
        reservPanel = new JScrollPane(reservTable);
        tableModel = new DefaultTableModel();
        
        reservTable.setModel(tableModel);
        for(i = 0 ; i < TABLE_COL_NUM; ++i){
            tableModel.addColumn(colNames[i]);
        }
        
        String [] emptyRow = new String[TABLE_COL_NUM];
        for(i = 0 ; i < TABLE_ROW_NUM; ++i){
            tableModel.addRow(emptyRow);
        }        
        
        reservPanel.setBounds(TABLE_START_X, TABLE_START_Y, TABLE_WIDTH, TABLE_HEIGHT); 

        this.add(reservPanel);

        //init information Area
        infoArea = new JTextArea();
        infoPanel = new JScrollPane(infoArea);
        
        infoPanel.setBounds(INFO_START_X, INFO_START_Y, INFO_WIDTH, INFO_HEIGHT);
    
        this.add(infoPanel);

        //init buttons
        
        addButton = new JButton(ADD_NAME);
        cancelButton = new JButton(CANCEL_NAME);
        checkButton = new JButton(CHECK_NAME);

        addButton.setBounds(ADD_START_X, ADD_START_Y, BUTTON_WIDTH, BUTTON_HEIGHT);
        cancelButton.setBounds(CANCEL_START_X, CANCEL_START_Y, BUTTON_WIDTH, BUTTON_HEIGHT);
        checkButton.setBounds(CHECK_START_X, CHECK_START_Y, BUTTON_WIDTH, BUTTON_HEIGHT);

        addButton.addActionListener(new AddButtonListener(infoArea));
        cancelButton.addActionListener(new CancelButtonListener());
        checkButton.addActionListener(new CheckButtonListener(infoArea, tableModel));

        this.add(addButton);
        this.add(cancelButton);
        this.add(checkButton);        
    }

    public static void main(String args[]){
        JFrame jf = new JFrame();
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainPanel jp = new MainPanel();
        jf.add(jp);
        jf.setSize(MainPanel.MAIN_WIDTH, MainPanel.MAIN_HEIGHT);
        jf.setResizable(false);
        jf.setVisible(true);
        
    }

}
