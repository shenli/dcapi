package dcapi.client;

import dcapi.util.*;
import dcapi.app.*;

import java.util.*;
import java.sql.Timestamp;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;

import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.CCNFileInputStream;
import org.ccnx.ccn.io.CCNInputStream;
import org.ccnx.ccn.io.CCNOutputStream;
import org.ccnx.ccn.protocol.CCNTime;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Exclude;
import org.ccnx.ccn.protocol.ExcludeComponent;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;


import org.ccnx.ccn.utils.CommonSecurity;


public class CCNQuerySender {

    private static final int BUF_SIZE = 8192;

    private ConfKeeper _conf = null;
    private Logger _logger = null;	

    private String _strPrefix = null;
    private ContentName _requestName = null;
    private ContentName _prefix = null;

    private RequestEncoder _enc = null;

    public CCNQuerySender(){
        this(new ConfKeeper(ConfKeeper.SELECT_CCN_PREFIX | 
                            ConfKeeper.SELECT_BASE_DIR |
                            ConfKeeper.SELECT_INVALID_CHAR));
    }

    public CCNQuerySender(ConfKeeper conf){

        this._conf = conf;
        _strPrefix = _conf.getCCNPrefix();
        _logger = new Logger("CCNQuerySender.log");
        _enc = new RequestEncoder(this._conf);
    }


    public Response sendQuery(Request request){
        try{
            String strQuest = _enc.encode(request);
            ContentName requestName = ContentName.fromURI(strQuest);
            System.out.println("**************" + requestName.toURIString() + "\n" + "***************" + _strPrefix + ", " + strQuest);
            CCNHandle handle = CCNHandle.open();
            CCNInputStream input = new CCNInputStream(requestName, handle);

            byte [] buffer = new byte[BUF_SIZE];
            ByteKeeper byteResponse = new ByteKeeper();

            int readCnt = 0;
            int readTotal = 0;
            _logger.write("Start Reading\n");
            readCnt = input.read(buffer);
            while(readCnt >= 0){
                _logger.write("In reading : readCnt is " + readCnt + "\n");
                byteResponse.append(buffer, readCnt);
                readTotal = readTotal + readCnt;
                readCnt = input.read(buffer);
            }
            input.close();
            handle.close();
            _logger.write("Read done!\n");
            System.out.println("Read Done!\n");
            String strResponse = new String(byteResponse.getBytes());
            System.out.println("Received string is : " + strResponse);
            //do not need a make valid function since there's no restrictions on the format of response
            return Response.deSerialize(strResponse);
        }
        catch (ConfigurationException e) {
            System.out.println("ConfigurationException in CCNQuerySender-sendQuery: " + e.getMessage());
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("IOException in CCNQuerySender-sendQuery: " + e.getMessage());
            e.printStackTrace();
        }
        catch (MalformedContentNameStringException e) {
            System.out.println("MalformedContentNameStringException in CCNQuerySender-sendQuery : " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
    
    public static void main(String argv[]){
        CCNQuerySender sender = new CCNQuerySender();
        Reservation reserv = Reservation.getRandReserv();
        
        long curTime = System.currentTimeMillis();
        reserv.setUserName("shenTest10");
        reserv.setStartTime(new Timestamp(curTime + 10*60*1000L));
        reserv.setEndTime(new Timestamp(curTime + 75*60*1000L));
        reserv.setUserEmail("geminialex007@gmail.com");
        Request request = new Request("add", reserv);
        sender.sendQuery(request);
        request = new Request("get", null);
        sender.sendQuery(request);
        System.out.println("done");
    }
}
