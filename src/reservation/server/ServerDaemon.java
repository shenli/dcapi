package dcapi.server;

import java.util.*;
import java.io.*;

import org.ccnx.ccn.protocol.*;
import org.ccnx.ccn.config.*;

import dcapi.util.*;

public class ServerDaemon{
    private static Logger _logger = null;
    

    //TODO: load the password information from an encryped file

    public static void main(String argv[]){
        ConfKeeper conf = new ConfKeeper();
        _logger = new Logger("ServerDaemon.log");
        String sudoPWD, emailPWD;
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please input the sudo password: ");
            sudoPWD = br.readLine();
            System.out.println("Please input the email password: ");
            emailPWD = br.readLine();
            br.close();
        }
        catch(Exception ex){
            _logger.write("Exception in ServerDaemon.Constructor : " + ex.getMessage());
            System.out.println("Exception in ServerDaemon.Constructor : " + ex.getMessage());
            ex.printStackTrace();
            return;
        }

        if(null == sudoPWD || null == emailPWD){
            System.out.println("PASSWORD ERROR\n");
            return;
        }

        try{
            CCNQueryListener queryListener = new CCNQueryListener(conf, emailPWD);
            AccessManager accManager = new AccessManager(conf, sudoPWD);
            queryListener.start();
            accManager.start();
        }
        catch(MalformedContentNameStringException ex){
            _logger.write("MalformedContentNameStringException in ServerDaemon-Main: " + ex.getMessage());
            ex.printStackTrace();
        }    
        catch(IOException ex){
            _logger.write("IOException in ServerDaemon-Main: " + ex.getMessage());
            ex.printStackTrace();
        }
        catch(ConfigurationException ex){
            _logger.write("ConfigurationException in ServerDaemon-Main: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
