package dcapi.server;

import dcapi.util.*;
import dcapi.database.*;

import java.sql.Timestamp;

public class AccessManager extends Thread{
    private ConfKeeper conf = null;
    private DBProxy dbConn = null;
    private Logger logger = null;
    private ShellExecutor shell = null;

    private int updateInterval = 10000;
    private String sudoPWD = null;


    public AccessManager(String sudoPWD){
        this(new ConfKeeper(), sudoPWD);
    }

    public AccessManager(ConfKeeper conf, String sudoPWD){
        this.sudoPWD = sudoPWD;
        this.conf = conf;
        dbConn = new DBProxy(this.conf);
        logger = new Logger("AccessManager.log");
        shell = new ShellExecutor(this.conf, this.sudoPWD);
        updateInterval = conf.getAMUpdateInterval();
        System.out.println(updateInterval);
    }

    public void run(){
        //TODO: take the setup time into account
        Reservation reserv = null;
        long curTime = 0, startTime = 0, endTime = 0;
        while(true){
            try{
                reserv = dbConn.getNextReservation();
                curTime = System.currentTimeMillis();

                // no reservation yet
                if(null == reserv){
                    Thread.sleep(this.updateInterval);
                }

                // remove deprecated reservation 
                while(reserv.getEndTime().getTime() <  curTime){
                    dbConn.mvToHistory(reserv.getReservID());
                    reserv = dbConn.getNextReservation();
                    curTime = System.currentTimeMillis(); 
                }
                //handle reservation
                startTime = reserv.getStartTime().getTime();
                curTime = System.currentTimeMillis();
                if(startTime <= curTime){
                    endTime = reserv.getEndTime().getTime();
                    shell.addUser(reserv.getUserName(), reserv.getUserPWD());
                    Thread.sleep(endTime - curTime);
                    shell.termUser(reserv.getUserName());
                    shell.delUser(reserv.getUserName());
                    dbConn.mvToHistory(reserv.getReservID());
                }
                // reservation is in near future, take a nap
                else if(startTime - curTime < this.updateInterval){
                    Thread.sleep(startTime - curTime);
                }
                // reservation is beyond next round
                else{
                    Thread.sleep(this.updateInterval);
                }
            }
            catch(InterruptedException ex){
                logger.write("InterruptedException in AccessManager.Run : " + ex.getMessage());
                System.out.println("InterruptedException in AccessManager.Run : " + ex.getMessage());
            }    
        }
    }
/* password removed
    public static void main(String argv[]){
        DBProxy dbConn = new DBProxy();
        System.out.println("0");
        AccessManager am = new AccessManager("password");
        System.out.println("1");
        Reservation reserv = Reservation.getRandReserv();
        System.out.println("2");
        reserv.setStartTime(new Timestamp(System.currentTimeMillis() + 10000));
        System.out.println("3");
        reserv.setEndTime(new Timestamp(System.currentTimeMillis() +   90000));
        System.out.println("4");
        dbConn.addReservation(reserv);
        System.out.println("5");
        am.start();
        System.out.println("6");
        reserv = Reservation.getRandReserv();
        System.out.println("7");
        reserv.setStartTime(new Timestamp(System.currentTimeMillis() + 110000));
        System.out.println("8");
        reserv.setEndTime(new Timestamp(System.currentTimeMillis() +   190000));
        System.out.println("9");
        dbConn.addReservation(reserv);
        System.out.println("0");
    }
*/
}
