package dcapi.server;

import java.util.*;
import java.lang.*;
import org.ccnx.ccn.io.CCNVersionedOutputStream;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.*;
import org.ccnx.ccn.profiles.CommandMarker;
import org.ccnx.ccn.profiles.SegmentationProfile;
import org.ccnx.ccn.profiles.VersioningProfile;
import org.ccnx.ccn.profiles.metadata.MetadataProfile;
import org.ccnx.ccn.profiles.nameenum.NameEnumerationResponse;
import org.ccnx.ccn.profiles.nameenum.NameEnumerationResponse.NameEnumerationResponseMessage;
import org.ccnx.ccn.profiles.nameenum.NameEnumerationResponse.NameEnumerationResponseMessage.NameEnumerationResponseMessageObject;
import org.ccnx.ccn.profiles.security.KeyProfile;
import org.ccnx.ccn.protocol.CCNTime;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Exclude;
import org.ccnx.ccn.protocol.ExcludeComponent;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;

import dcapi.app.*;
import dcapi.util.*;

public class CCNQueryListener implements CCNFilterListener{
    private static final int BUF_SIZE = 4096;

    private String _baseDir = null;
    private String _strPrefix = null;
    private Interest _interest = null;
    private CCNHandle _handle = null;
    private ContentName _prefix = null;
    private Logger _logger = null;
    private ConfKeeper _conf = null;
    private SynHashSet _interestCache = null;

    private String _emailPWD = null;

    
    public CCNQueryListener(String emailPWD) throws MalformedContentNameStringException, ConfigurationException, IOException {
        this(new ConfKeeper(), emailPWD);
    }

    public CCNQueryListener(ConfKeeper conf, String emailPWD) throws MalformedContentNameStringException, ConfigurationException, IOException {
        _emailPWD = emailPWD;
        _conf = conf;
        _logger = new Logger("CCNQueryListener.log");        
        _baseDir = _conf.getBaseDir();
        _strPrefix = _conf.getCCNPrefix();
        _prefix = ContentName.fromURI(_strPrefix);
        _handle = CCNHandle.open();
        _interestCache = new SynHashSet();
    }


    private boolean transmitData(ByteKeeper response, Interest interest, CCNHandle handle){
        //CCNTime birthTime = new CCNTime(data.getBirthTime());

        try{
            CCNOutputStream ccnout = new CCNOutputStream(interest.name(), handle);

            //register the outstanding interest
            ccnout.addOutstandingInterest(interest);

            byte [] buffer = new byte[BUF_SIZE];
            int offset = 0;
            int readCnt = response.read(buffer, BUF_SIZE, offset);
            _logger.write("Start sending data\n");
            while (readCnt >0){
                ccnout.write(buffer, 0, readCnt);
                _logger.write("Sending data, readCnt = " + readCnt + "\n");
                offset = offset + readCnt;
                readCnt = response.read(buffer, BUF_SIZE, offset);
            }
            _logger.write("Data Transmission done!\n");
            ccnout.close();
        }
        catch(IOException ex){
            _logger.write("In transmitData: IOException: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        return true;

    }

    public void start() throws IOException{
        _logger.write("QueryListern has started!\n");
        // All we have to do is say that we're listening on our main prefix.
        _handle.registerFilter(_prefix, this);
    }



    public boolean handleInterest(Interest interest) {
        // Alright, we've gotten an interest. Either it's an interest for a stream we're
        // already reading, or it's a request for a new stream.
        _logger.write("received Interest : " + interest.name().toURIString() + "\n");

        
        if(!(_interestCache.add(interest.name().toURIString() ))){
            _logger.write("Duplicate interest : " + interest.name().toURIString());
            return false;
        }
        else if (SegmentationProfile.isSegment(interest.name()) && !SegmentationProfile.isFirstSegment(interest.name())) {
            _logger.write("Got an interest for something other than a first segment, ignoring : " + interest.name().toURIString());
            return false;
        } 
        else if (MetadataProfile.isHeader(interest.name())) {
            _logger.write("Got an interest for the first segment of the header, ignoring : " + interest.name().toURIString());
            return false;
        }
        //TODO: check duplicate interest
        RequestDecoder dec = new RequestDecoder(this._conf);
        Request request = dec.decode(interest.name().toURIString());
        RequestHandler reqHandler = new RequestHandler(this._conf, _emailPWD);
        Response response = reqHandler.handle(request);
        String strResponse = response.serialize();
        ByteKeeper byteResponse = new ByteKeeper(strResponse.getBytes());
        return transmitData(byteResponse, interest, _handle);
    }



    /**
     * Turn off everything.
     * @throws IOException 
     */
    public void shutdown() throws IOException {
        if (null != _handle) {
            _handle.unregisterFilter(_prefix, this);
            _logger.write("CCNQueryListener Closed!\n");
        }
    }




}


