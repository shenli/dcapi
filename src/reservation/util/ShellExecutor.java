package dcapi.util;

import java.util.*;
import java.io.*;
import com.google.gson.Gson;

public class ShellExecutor{

    private static final String addUserFormat = "/bin/echo %s | /usr/bin/sudo -S /bin/sh -c " + //sudo password
        "\"/usr/sbin/useradd %s -d /home/tarek/%s -g dcapi; " + //username, username
        "/bin/echo -e \\\"%s\\n%s\\\" | " + //userpwd, userpwd
        "/usr/bin/passwd %s; " +  //username
        "chown %s:dcapi /home/tarek/%s\""; //username, username

    private static final String delUserFormat = "/bin/echo %s | /usr/bin/sudo -S /bin/sh -c " + //sudo password
        "\"/usr/sbin/userdel -r %s;\"";  //username

    private static final String termUserFormat = "/bin/echo %s | /usr/bin/sudo -S /bin/sh -c " + //sudo password
        "\"/usr/bin/killall -u %s\""; //username


    private String sudoPWD = null;
    private ConfKeeper conf = null;
    private HashSet<String> workerList = null;
    private Logger logger = null;
    private Gson gson = null;

    public ShellExecutor(ConfKeeper conf, String sudoPWD){
        this.conf = conf;
        this.sudoPWD = sudoPWD;
        gson = new Gson();
        workerList = conf.getWorkerList();
        logger = new Logger("ShellExecutor.log");
    }

    public ShellExecutor(String sudoPWD){
        this(new ConfKeeper(), sudoPWD);
    }

    private boolean execute(String [] cmd, boolean wait){
        String jsonCMD = gson.toJson(cmd);

        try{
            Process p = Runtime.getRuntime().exec(cmd);
            String s = null;
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // read the output from the command
            System.out.println("Here is the standard output of the command: " + jsonCMD + "\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

            // read any errors from the attempted command
            System.out.println("Here is the standard error of the command in ShellExecutor(if any): " + jsonCMD + "\n");
            logger.write("Here is the standard error of the command in ShellExecutor(if any): " + jsonCMD + "\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
                logger.write(s);
            }

            if(wait){
                p.waitFor();
            }

            return true;
        }
        catch(Exception ex){
            logger.write("Exception In ShellExecutor.execute : \n" +
                    "ERROR CMD is :" + jsonCMD +"\n" + 
                    "Exception message is : " + ex.getMessage());

            System.out.println("Exception In ShellExecutor.execute : \n" +
                    "ERROR CMD is :" + jsonCMD +"\n" + 
                    "Exception message is : " + ex.getMessage());
            ex.printStackTrace();
        }

        return false;
    }


    public boolean addUser(String usr, String pwd){
        System.out.println("In add User : " + usr + ", " + pwd);
        Iterator<String> iter = workerList.iterator();
        String curWorker = null;
        String strAddUser = String.format(addUserFormat, sudoPWD, usr, usr, pwd, pwd, usr, usr, usr);

        boolean successFlag = true;
        while(iter.hasNext()){
            try{
                curWorker = iter.next();
                System.out.println(">>>>>>>>>nodes and command are <<<<<<<<:" + curWorker + ", " + strAddUser);
                String [] cmd = {"/usr/bin/ssh", curWorker,  strAddUser};
                if(!execute(cmd, false)){
                    successFlag = false;
                }
            }
            catch(Exception ex){
                logger.write("Exception in ShellExecutor.addUser : " + ex.getMessage());
                System.out.println("Exception in ShellExecutor.addUser : " + ex.getMessage());
                ex.printStackTrace();
                successFlag =  false;
            }    
        }
        return successFlag;
    }

    public boolean delUser(String usr){
        System.out.println("In del User : " + usr );

        Iterator<String> iter = workerList.iterator();
        String curWorker = null;
        String strDelUser = String.format(delUserFormat, sudoPWD, usr);

        boolean successFlag = true;

        while(iter.hasNext()){
            try{
                curWorker = iter.next();
                System.out.println(curWorker + ", " + strDelUser);
                String [] cmd = {"/usr/bin/ssh", curWorker,  strDelUser};
                if(!execute(cmd, false)){
                    successFlag = false;
                }
            }
            catch(Exception ex){
                logger.write("Exception in ShellExecutor.delUser : " + ex.getMessage());
                System.out.println("Exception in ShellExecutor.delUser : " + ex.getMessage());
                ex.printStackTrace();
                successFlag = false;
            }
        }

        return successFlag;
    }

    public boolean termUser(String usr){
        System.out.println("In term User : " + usr );

        Iterator<String> iter = workerList.iterator();
        String curWorker = null;
        String strTermUser = String.format(termUserFormat, sudoPWD, usr);

        boolean successFlag = true;

        while(iter.hasNext()){
            try{
                curWorker = iter.next();
                System.out.println(curWorker + ", " + strTermUser);
                String [] cmd = {"/usr/bin/ssh", curWorker,  strTermUser};
                if(!execute(cmd, true)){
                    successFlag = false;
                }
            }
            catch(Exception ex){
                logger.write("Exception in ShellExecutor.delUser : " + ex.getMessage());
                System.out.println("Exception in ShellExecutor.delUser : " + ex.getMessage());
                ex.printStackTrace();
                successFlag = false;
            }
        }

        return successFlag;
    }

/* password removed
    public static void main(String argv[]){
        String usr = "shenTest9";
        ShellExecutor shell = new ShellExecutor("password");
        shell.addUser(usr, "1234abcd");
        try{
            Thread.sleep(300000);
        }
        catch(Exception ex){
            System.out.println("Sleep Exception : " + ex.getMessage());
            ex.printStackTrace();
        }   
        shell.termUser(usr); 
        shell.delUser(usr);
    }
*/
}
