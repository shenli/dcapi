package dcapi.util;


import java.util.*;
import java.util.regex.Pattern;

public class StrValidator{
    private ConfKeeper conf = null;
    private Hashtable<Character, String> inChar2str = null;
    private Hashtable<String, Character> str2inChar = null;

    public StrValidator(ConfKeeper conf){
        this.conf = conf;
        inChar2str = conf.getInChar2Str();
        str2inChar = conf.getStr2InChar();
    }

    public StrValidator(){
        this(new ConfKeeper());
    }

    public String toValid(String str){
        Iterator<Character> iter = inChar2str.keySet().iterator();
        Character oriChar = null;
        while(iter.hasNext()){
            oriChar = iter.next();
            String ori = oriChar.toString();
            String res = inChar2str.get(oriChar);
            str = str.replaceAll(Pattern.quote(ori), res);
        }
        return str;
    }

    public String fromValid(String str){
        Iterator<String> iter = str2inChar.keySet().iterator();
        while(iter.hasNext()){
            String ori = iter.next();
            String res = str2inChar.get(ori).toString();
            str = str.replaceAll(Pattern.quote(ori), res);
        }
        return str;
    }

}

