package dcapi.util;

import java.io.*;
import java.util.*;
import com.google.gson.Gson;


public class ConfKeeper{
    public static final long SELECT_BASE_DIR = 1 << 0;
    public static final long SELECT_ADMIN_DIR = 1 << 1;
    public static final long SELECT_INVALID_CHAR = 1 << 2;
    public static final long SELECT_DB_CONF = 1 << 3;
    public static final long SELECT_ACCESS_CONF = 1 << 4;
    public static final long SELECT_EMAIL_CONF = 1 << 5;
    public static final long SELECT_WORKER_LIST = 1 << 6;
    public static final long SELECT_USER_LIST = 1 << 7;
    public static final long SELECT_CCN_PREFIX = 1 << 8;
    public static final long SELECT_ALL = SELECT_BASE_DIR | SELECT_ADMIN_DIR | SELECT_INVALID_CHAR | SELECT_DB_CONF |
        SELECT_ACCESS_CONF | SELECT_WORKER_LIST | SELECT_USER_LIST | SELECT_CCN_PREFIX | SELECT_EMAIL_CONF; 

    private static final String homeEnv = "DCAPI_HOME";
    private static final String adminHomeEnv = "HOME";
    private String baseDir = null;
    private String adminDir = null;

    private static final String ccnPrefixPath = "/conf/prefix"; 
    private String ccnPrefix = null; 

    private static final String invalidCharPath = "/conf/invalid_char"; 
    private Hashtable<Character, String> inChar2str = null;
    private Hashtable<String, Character> str2inChar = null;


    private static final String dbConfPath = "/conf/db_conf";
    private Hashtable<String, String> dbConf = null;

    private static final String accessConfPath = "/conf/access_conf";
    private Hashtable<String, Integer> accessConf = null;

    private static final String emailConfPath = "/conf/email_conf";
    private Hashtable<String, String> emailConf = null;

    private static final String workerListPath = "/conf/workers";
    private HashSet<String> workerList = null;

    private static final String userListPath = "/config/userList";
    private HashSet<String> userList = null;

    public ConfKeeper(){
        this(SELECT_ALL);
    }

    public ConfKeeper(long select){
        FileReader fr = null;
        BufferedReader br = null;
        try{
            //get base directory
            if((select & SELECT_BASE_DIR) != 0){
                baseDir = System.getenv(homeEnv);
            }
            if((select & SELECT_ADMIN_DIR) != 0){
                adminDir = System.getenv(adminHomeEnv);            
            }

            //get ccn app prefix
            if((select & SELECT_CCN_PREFIX) != 0){
                fr = new FileReader(baseDir + ccnPrefixPath);
                br = new BufferedReader(fr);
                ccnPrefix = br.readLine();
                br.close();
                if(ccnPrefix.charAt(ccnPrefix.length() - 1) != '/'){
                    ccnPrefix = ccnPrefix + "/";
                }
            }
            //get ccn invalid characters
            if((select & SELECT_INVALID_CHAR) != 0){
                fr = new FileReader(baseDir + invalidCharPath);
                br = new BufferedReader(fr);
                inChar2str = new Hashtable<Character, String>();
                str2inChar = new Hashtable<String, Character>();
                int cnt = 0;
                String validStr = null;
                Character inChar = null;
                while(br.ready()){
                    String line = br.readLine();
                    //System.out.println(line);
                    validStr = ".~" + cnt + "~.";
                    inChar = new Character(line.charAt(0));
                    inChar2str.put(inChar, validStr);
                    str2inChar.put(validStr, inChar);
                    ++cnt;
                }
                br.close();
                Gson gson = new Gson();
                //System.out.println(gson.toJson(inChar2str));
                //System.out.println(gson.toJson(str2inChar));
            }



            //get db configuration
            if((select & SELECT_DB_CONF) != 0){
                dbConf = new Hashtable<String, String>();
                fr = new FileReader(baseDir + dbConfPath);
                br = new BufferedReader(fr);
                while(br.ready()){
                    String line = br.readLine();
                    String [] items = line.split(" += +");
                    dbConf.put(items[0], items[1]);
                }
                br.close();
                //System.out.println(gson.toJson(dbConf));
            }


            //get access configuration
            if((select & SELECT_ACCESS_CONF) != 0){
                accessConf = new Hashtable<String, Integer>();
                fr = new FileReader(baseDir + accessConfPath);
                br = new BufferedReader(fr);
                while(br.ready()){
                    String line = br.readLine();
                    String [] items = line.split(" += +");
                    accessConf.put(items[0], new Integer(items[1]));  
                }
                br.close();
                //System.out.println(gson.toJson(accessConf)); 
            }

            //get workerList
            if((select & SELECT_WORKER_LIST) != 0){
                workerList = new HashSet<String>();
                fr = new FileReader(baseDir + workerListPath);
                br = new BufferedReader(fr);
                while(br.ready()){
                    String line = br.readLine();
                    workerList.add(line);
                }
                br.close();
                //System.out.println(gson.toJson(workerList));
            }
            //get email configuration
            if((select & SELECT_EMAIL_CONF) != 0){
                emailConf = new Hashtable<String, String>();
                fr = new FileReader(baseDir + emailConfPath);
                br = new BufferedReader(fr);
                while(br.ready()){
                    String line = br.readLine();
                    String [] items = line.split(" += +");
                    emailConf.put(items[0], items[1]);
                }
                br.close();
                //System.out.println(gson.toJson(emailConf));
            }

            //get user list
            if((select & SELECT_USER_LIST) != 0){
                userList = new HashSet<String>();
                fr = new FileReader( adminDir + userListPath);
                br = new BufferedReader(fr);
                while(br.ready()){
                    String line = br.readLine();
                    userList.add(line);
                }
                br.close();
                //System.out.println(gson.toJson(userList));
            }

        }
        catch(IOException ex){
            System.out.println("In ccnGetPrefix : IOException : " + ex.getMessage());
            ex.printStackTrace();
        }
        catch(Exception ex){
            System.out.println("In ccnGetPrefix : Exception : " + ex.getMessage());
            ex.printStackTrace();
        }

    }

    public boolean hasUser(String usr){
        return userList.contains(usr);
    }

    public HashSet<String> getWorkerList(){
        return (HashSet<String>)workerList.clone();
    }

    public String getEmailServer(){
        return emailConf.get("server");
    }

    public String getEmailUsr(){
        return emailConf.get("usr");
    }

    public String getBaseDir(){
        return this.baseDir;
    }

    public String getCCNPrefix(){
        return this.ccnPrefix;
    }

    public Hashtable<Character, String> getInChar2Str(){
        return this.inChar2str;
    }    

    public Hashtable<String, Character> getStr2InChar(){
        return this.str2inChar;
    }

    public String getDBUsr(){
        return dbConf.get("usr");
    }

    public String getDBPwd(){
        return dbConf.get("pwd");
    }

    public String getDBDriverName(){
        return dbConf.get("driverName");
    }

    public String getDBUrlReserv(){
        return dbConf.get("urlReserv");
    }

    public String getDBUrlHistory(){
        return dbConf.get("urlHistory");
    }

    public int getAMUpdateInterval(){
        return accessConf.get("updateInterval").intValue();
    }

    public int getAMSetupTime(){
        return accessConf.get("setupTime").intValue();
    }

    public static void main(String argv[]){
        ConfKeeper conf = new ConfKeeper();
        System.out.println(conf.getBaseDir() + "\n" + conf.getCCNPrefix());
    }
}
