package dcapi.util;

import org.apache.commons.mail.*;
import javax.mail.*;

public class EmailProxy{
    private ConfKeeper conf = null;
    private Logger logger = null;
    private Email email = null;
    private String usr = null;
    private String pwd = null;
    private String server = null;

    public EmailProxy(String pwd){
        this(new ConfKeeper(), pwd);
    }

    public EmailProxy(ConfKeeper conf, String pwd){
        this.conf = conf;
        this.logger = new Logger("EmailProxy.log");
        this.server = conf.getEmailServer();
        this.usr = conf.getEmailUsr();
        this.pwd = pwd;

        if(0 == this.server.compareTo("gmail.com")){
            try{    
                email = new SimpleEmail();
                email.setHostName("smtp.gmail.com");
                email.setSmtpPort(587);
                email.setAuthenticator(new DefaultAuthenticator(this.usr, this.pwd));
                email.setTLS(true);
                email.setFrom(usr + "@gmail.com");
            }
            catch(EmailException ex){
                logger.write("EmailException in EmailProxy.Constructor : " + ex.getMessage());
                System.out.println("EmailException in EmailProxy.Constructor : " + ex.getMessage());
                ex.printStackTrace();
            }    
        }
        else{
            logger.write("Invalid server : " + server);
            System.out.println("Invalid server : " + server);
        }
    }

    public boolean sendEmail(String to, String title, String msg){
        if(null == this.email){
            logger.write("email instance not initialized, potentially with a invalid server : " + server);
            System.out.println("email instance not initialized, potentially with a invalid server : " + server);
            return false;
        }
        try{
            email.setSubject(title);
            email.setMsg(msg);
            email.addTo(to);
            email.send();
            return true;
        }
        catch(EmailException ex){
            logger.write("EmailException in EmailProxy.send : " + ex.getMessage());
            System.out.println("EmailException in EmailProxy.send : " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }

    }
/* email password hide
    public static void main(String args[]){
        EmailProxy emailProxy = new EmailProxy("email password");
        emailProxy.sendEmail("geminialex007@gmail.com", "test dcapi email", "test test");
    }
*/
}
