package dcapi.util;

import java.util.*;

public class SynHashSet{
    private HashSet<String> hashSet = null;

    public SynHashSet(){
        hashSet = new HashSet<String>();
    }

    public synchronized boolean add(String str){
        return hashSet.add(str);
    }

    //TODO: implement a LRU remove algorithm or use some library

    public static void main(String argv[]){
        SynHashSet synHashSet = new SynHashSet();
        String a = "abcd123";
        String b = new String ("abcd123");
        System.out.println((a == b));
        System.out.println(synHashSet.add(a));
        System.out.println(synHashSet.add(b));
    }
}
