package dcapi.util;

import java.sql.Timestamp;
import java.util.*;
import com.google.gson.Gson;

public class ReservationList{


    public static final Reservation minReserv = new Reservation(Reservation.minTime, new Timestamp(Reservation.minTime.getTime() + Reservation.oneDay));
    public static final Reservation maxReserv = new Reservation(Reservation.maxTime, new Timestamp(Reservation.maxTime.getTime() + Reservation.oneDay));
    private ArrayList<Reservation> reservList = null;
    
    /**
     * The current implementation is O(n). Can be improved to O(lg n) by binary search.
     */
    private int checkValid(Reservation reserv){
        if(null == reserv){
            return -2;
        }
        if(reserv.compareTo(ReservationList.minReserv) < 0 || reserv.compareTo(ReservationList.maxReserv) > 0){
            return -1;
        }
        if(this.reservList.size() <= 0){
            return 0;
        }
        Reservation curReserv = null;
        Iterator<Reservation> iter = this.reservList.iterator();
        int index = 0;
        while(iter.hasNext()){
            curReserv = iter.next();
            if(curReserv.compareTo(reserv) > 0){
                return index;
            }
            else if (curReserv.compareTo(reserv) < 0){
                index++;
            }
            else{
                return -1;
            }            
        }
        
        return this.reservList.size();
    }

    public static boolean checkValid(ArrayList<Reservation> reservList){
        if(null == reservList)
            return false;
        Iterator<Reservation> iter = reservList.iterator();
        Timestamp curTime = Reservation.minTime;
        Timestamp nextTime = null;
        while(iter.hasNext()){
            nextTime = iter.next().getStartTime();
            if(curTime.compareTo(nextTime) > 0){
                return false;
            }
            curTime = nextTime;
        }
        return true;
    }

    public ReservationList(){
        this.reservList = new ArrayList<Reservation>();
    }

    public ReservationList(ReservationList reservList){
        //TODO: make a copy of the input to prevent invalid modification after construction
        this.reservList = new ArrayList<Reservation>();
        int len = reservList.size();
        for(int i = 0 ; i < len; ++i){
            this.reservList.add(reservList.get(i));
        }
    }

    public int size(){
        if(null == this.reservList){
            return -1;
        }
        else{
            return this.reservList.size();
        }
    }

    public Reservation get(int index) throws IndexOutOfBoundsException{
        if(null == this.reservList){
            return null;
        }
        else{
            return new Reservation(this.reservList.get(index));
        }
        
    }

    public boolean setReservList(ArrayList<Reservation> reservList){
        if(checkValid(reservList)){
            this.reservList = reservList;
            return true;
        }
        else{
            return false;
        }
        
    }

    /**
     * Add one reservation to the list while keeps the order and validation of reservations.
     * @param reserv the new reservation trying to make.
     */
    public boolean add(Reservation reserv){
        int index = checkValid(reserv);
        if(index >= 0){
            //Need to keep the order of reservation
            this.reservList.add(index, reserv);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Remove the first reservation (should be the earlest reservation) in the list.
     */
    public boolean remove(){
        if(this.reservList.size() <=0){
            return false;
        }
        else{
            this.reservList.remove(0);
            return true;
        }
    }

    /**
     * Remove existing reservations that violates with the new reservation. (Not implementated yet)
     */
    public boolean remove(Reservation reserv){
        return false;
    }


    public String serialize(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }    

    public static ReservationList deSerialize(String data){
        try{
            Gson gson = new Gson();
            return gson.fromJson(data, ReservationList.class);
        }
        catch(Exception ex){
            return null;
        }
    }

    public static void main(String argv[]){
        Reservation reserv = new Reservation(new Timestamp(Reservation.minTime.getTime() + 2 * Reservation.oneDay), 
                    new Timestamp(Reservation.minTime.getTime() + 3 * Reservation.oneDay));
        ReservationList reservList = new ReservationList();
        System.out.println(reservList.serialize());
        reservList.add(reserv);
        System.out.println(reservList.serialize());
        System.out.println("Start random test");
        Random rand = new Random();
        long startShift = 0, duration = 0;
        long maxShift = 10L * 365L * 24L * 60L * 60L * 1000L;
        System.out.println(maxShift + ", " + Reservation.oneDay);
        System.out.println("Max shit is : " + (maxShift / Reservation.oneDay) + "Day");
        for(int i = 0 ; i < 20; ++i){
            startShift = Math.abs(rand.nextLong()) % maxShift;
            duration = Math.abs(rand.nextLong()) % maxShift;
            System.out.println(startShift / Reservation.oneDay + ", " + duration / Reservation.oneDay);
            reserv = new Reservation(new Timestamp(Reservation.minTime.getTime() + startShift),
                    new Timestamp(Reservation.minTime.getTime() + startShift + duration));
            System.out.println("Reservation is : " + reserv.serialize());
            System.out.println("i = " + i + ", " + reservList.add(reserv));
            System.out.println(reservList.serialize());
        }
        ReservationList reservList1 = new ReservationList(reservList);
        ArrayList<Reservation> arr = new ArrayList<Reservation>();
        reservList.setReservList(arr);
        System.out.println(reservList.serialize());
        System.out.println(reservList1.serialize());

    }

}

