package dcapi.util;

import java.sql.Timestamp;

public class EmailWriter{
    private static final String notifyTitle = "UIUC Green Server Farm Reservation Notification";
    private static final String notifyFormat = "Dear UIUC Green Server Farm user:\n" +
                                        "Thanks for using our cluster! Below is your reservation information:\n\n" +
                                        "Reservation ID : %s\n" +   // reservation id
                                        "Login User Name : %s\n" +  // user name
                                        "Login Password : %s\n" +    // user password
                                        "Start Time : %s\n" +       // reservation start time
                                        "End Time : %s\n\n" +       // reservation end time
                                        "The hostname for you to login is tareka01.cs.uiuc.edu. " + 
                                        "Please note that your username and password will not be activated util your reserved time slot" + 
                                        "and will be deactivated when your reservation expires. Your home folder will be /home/tarek/%s. " + // user name
                                        "Your home folder is using NFS on all servers in our cluster, and the space is very limited for the home folder. " +
                                        "Therefore, if you have large files, please use /scratch/ on each server's local disk instead. " +
                                        "Your home folder will be deleted when your reservation expires. " +
                                        "Thus, please remember to back up your files when needed.\n\n" +
                                        "If you have any question regarding this testbed, please contact Shen Li (shenli3 at illinois dot edu).\n" +
                                        "Best Regards,\n" +
                                        "UIUC Green Server Farm Administrator";
    
    public static String getNotifyMsg(Reservation reserv){
        Timestamp startTime = reserv.getStartTime();
        Timestamp endTime = reserv.getEndTime();
        String userName = reserv.getUserName();
        String userPWD = reserv.getUserPWD();
        long reservID = reserv.getReservID();
        String msg = String.format(notifyFormat, String.valueOf(reservID),
                                    userName, userPWD,
                                    startTime.toString(), endTime.toString(),
                                    userName);
        return msg;
        
    }

    public static String getNotifyTitle(){
        return notifyTitle;
    }

/* email password hide
    public static void main(String argv[]){
        Reservation reserv = Reservation.getRandReserv();
        reserv.setUserEmail("geminialex007@gmail.com");
        String msg = EmailWriter.getNotifyMsg(reserv);
        EmailProxy email = new EmailProxy("email_password");
        email.sendEmail(reserv.getUserEmail(), EmailWriter.getNotifyTitle(), msg);
    }
*/
}
