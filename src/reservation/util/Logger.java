package dcapi.util;

import java.io.*;
import java.util.*;
import java.text.*;


public class Logger{
    private String homeEnv = "DCAPI_HOME";
    private String baseDir = null;
    private String fileName = null;
    private File file = null;
    private Calendar cal = null;
    private DateFormat dateFormat = null;
    public Logger(String fileName){
        this.baseDir = System.getenv(homeEnv);
        this.fileName = baseDir + "/log/" + fileName;
        System.out.println(this.fileName);
        try{
            this.dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            this.cal = Calendar.getInstance();
            this.file = new File(this.fileName);
        }
        catch(Exception ex){
            System.out.println("In Logger-Constructor Exception : " + ex.getMessage());
            ex.printStackTrace();
        }

    }

    public void write(String msg){
        try{
            FileWriter fileWriter = new FileWriter(this.file, true);
            fileWriter.write(dateFormat.format(cal.getTime()) + " --- " + msg + "\n");
            fileWriter.close();

        }
        catch(Exception ex){
            System.out.println("Exception in Logger-write : " + ex.getMessage());
            ex.printStackTrace();
        }
    }


    public static void main(String args[]){
        Logger log = new Logger("test.log");
        log.write("first test");
        try{
            Thread.currentThread().sleep(1000);
        }
        catch(Exception ex){
            System.out.println("Exception in sleep : " + ex.getMessage());
            ex.printStackTrace();
        }  
        log.write("111111\n");
        log.write("66666");
    }
}
