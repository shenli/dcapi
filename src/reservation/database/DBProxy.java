package dcapi.database;

import java.sql.*;
import java.util.*;
import com.google.gson.Gson;

import dcapi.util.*;

public class DBProxy{

    private ConfKeeper conf = null;
    private String driverName = null;
    private String urlReserv = null;
    private String urlHistory = null;
    //private static final String url = "jdbc:mysql://localhost/dcapi";
    private String usr = null;
    private String pwd = null;

    //sql to get all reservations
    private static final String qGetAllReserv = "select start_time, end_time from reservation";
    //sql to get future reservations (new select now())
    private static final String qGetFutureReserv = "select reserv_id, user_name, start_time, end_time from reservation where start_time > (select now())";
    //sql to get future reservation for update
    private static final String qGetFutureReservLock = "select start_time, end_time from reservation where start_time > (select now()) FOR UPDATE";
    //sql to get conflicts: newStartTime in [start_time, end_time] or start_time in [newStartTime, newEndTime]
    private static final String qGetConflictLock = "select reserv_id, start_time, end_time from reservation where" +
        "(start_time >= ? and start_time <= ?) or (? >= start_time and ? <= end_time)";
    //sql to add one reservation
    private static final String qAddReserv = "insert into reservation " +
        "(user_name, user_pwd, user_email, start_time, end_time) " +
        "values(?, ?, ?, ?, ?)";
    //sql to get the current next reservation
    private static final String qGetNextReserv = "select * from reservation order by start_time ASC limit 1";
    //sql to drop one reservation with given reservation id
    private static final String qDelReserv = "delete from reservation where reserv_id = ?";
    //sql to get one reservation with given reservation id
    private static final String qGetOneReserv = "select * from reservation where reserv_id = ?";

    //sql to add one reservation to history
    private static final String qAddHistory = "insert into history " +
        "(user_name, user_pwd, user_email, start_time, end_time, reserv_id) " +
        "values(?, ?, ?, ?, ?, ?)";

    private static final String qBegin = "BEGIN";
    private static final String qCommit = "COMMIT";


    private Connection connHistory = null;
    private Connection connReserv = null;
    private PreparedStatement getAllReserv = null;
    private PreparedStatement getConflictLock = null;
    private PreparedStatement getFutureReserv = null;
    private PreparedStatement getFutureReservLock = null;
    private PreparedStatement addReserv = null;
    private PreparedStatement getNextReserv = null;
    private PreparedStatement delReserv = null;
    private PreparedStatement getOneReserv = null;

    private PreparedStatement addHistory = null;

    private Statement stmtReserv = null;    
    private Statement stmtHistory = null;
    private Logger logger = null;

    public DBProxy(){
        this(new ConfKeeper());
    }

    public DBProxy(ConfKeeper conf){
        logger = new Logger("DBProxy.log");
        //get configuration
        this.conf = conf;
        driverName = conf.getDBDriverName();
        urlReserv = conf.getDBUrlReserv();
        urlHistory = conf.getDBUrlHistory();
        usr = conf.getDBUsr();
        pwd = conf.getDBPwd();
        //init connection
        try{
            Class.forName(driverName);
            connReserv = DriverManager.getConnection(urlReserv, usr, pwd);
            connHistory = DriverManager.getConnection(urlHistory, usr, pwd);
            getAllReserv = connReserv.prepareStatement(qGetAllReserv);
            getFutureReserv = connReserv.prepareStatement(qGetFutureReserv);
            getConflictLock = connReserv.prepareStatement(qGetConflictLock);
            getFutureReservLock = connReserv.prepareStatement(qGetFutureReservLock);
            addReserv = connReserv.prepareStatement(qAddReserv);
            getNextReserv = connReserv.prepareStatement(qGetNextReserv);
            delReserv = connReserv.prepareStatement(qDelReserv);
            getOneReserv = connReserv.prepareStatement(qGetOneReserv);

            stmtReserv = connReserv.createStatement();

            addHistory = connHistory.prepareStatement(qAddHistory);

            stmtHistory = connHistory.createStatement();
        }
        catch(Exception ex){
            logger.write("Exception in Constructor: " + ex.getMessage());
            System.out.println("Exception in Constructor: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Thread safe insert by using transaction on InnoDB table
     */
    public boolean addReservation(Reservation reserv){
        //use select to see whether it conflicts
        try{
            getConflictLock.setTimestamp(1, reserv.getStartTime());
            getConflictLock.setTimestamp(2, reserv.getEndTime());
            getConflictLock.setTimestamp(3, reserv.getStartTime());
            getConflictLock.setTimestamp(4, reserv.getStartTime());

            addReserv.setString(1, reserv.getUserName());
            addReserv.setString(2, reserv.getUserPWD());
            addReserv.setString(3, reserv.getUserEmail());
            addReserv.setTimestamp(4, reserv.getStartTime());
            addReserv.setTimestamp(5, reserv.getEndTime());

            stmtReserv.execute(qBegin);

            ResultSet rs = getConflictLock.executeQuery();
            if(rs.next()){
                rs.close();
                stmtReserv.execute(qCommit);
                System.out.println("Conflict found!");
                return false;
            }

            addReserv.executeUpdate();

            stmtReserv.execute(qCommit);
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.addReservation : " + ex.getMessage());
            System.out.println("Exception in DBProxy.addReservation : " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean addHistory(Reservation reserv){
        try{
            addHistory.setString(1, reserv.getUserName());
            addHistory.setString(2, reserv.getUserPWD());
            addHistory.setString(3, reserv.getUserEmail());
            addHistory.setTimestamp(4, reserv.getStartTime());
            addHistory.setTimestamp(5, reserv.getEndTime());
            addHistory.setLong(6, reserv.getReservID());
            addHistory.executeUpdate();
            return true;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.addHistory : " + ex.getMessage());
            System.out.println("Exception in DBProxy.addHistory : " + ex.getMessage());
            ex.printStackTrace();
            return false;

        }
    }

    public Reservation getOneReservation(long reservID){
        try{
            getOneReserv.setLong(1, reservID);
            ResultSet rs = getOneReserv.executeQuery();
            Reservation reserv = extractReservation(rs);
            rs.close();
            return reserv;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.getOneReservation : " + ex.getMessage());
            System.out.println("Exception in DBProxy.getOneReservation : " + ex.getMessage());
            ex.printStackTrace();
            return null;

        }
    }

    public boolean mvToHistory(long reservID){
        try{
            getOneReserv.setLong(1, reservID);

            stmtReserv.execute(qBegin);
            ResultSet rs = getOneReserv.executeQuery();
            Reservation reserv = extractReservation(rs);
            if(null == reserv){
                return false;
            }
            addHistory(reserv);
            delReservation(reservID);
            stmtReserv.execute(qCommit);
            rs.close();
            return true;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.mvToHistory : " + ex.getMessage());
            System.out.println("Exception in DBProxy.mvToHistory : " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Only reservation time interval will be returns. 
     */
    public ReservationList getReservationList(){
        try{
            ResultSet rs = getFutureReserv.executeQuery();
            Reservation reserv = null;
            ReservationList reservList = new ReservationList();
            Timestamp startTime = null, endTime = null;
            String userName = null;
            long reservID = 0;
            while(rs.next()){
                startTime = rs.getTimestamp("start_time");
                endTime = rs.getTimestamp("end_time");
                reservID = rs.getLong("reserv_id");
                userName = rs.getString("user_name");

                reserv = new Reservation(startTime, endTime, reservID, userName, null, null);
                reservList.add(reserv);
                reserv = null;
            }
            rs.close();
            return reservList;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.getReservationInterval : " + ex.getMessage());
            System.out.println("Exception in DBProxy.getReservationInterval : " + ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    private Reservation extractReservation(ResultSet rs){
        try{
            if(!(rs.next()))
                return null;
            Timestamp startTime = null, endTime = null;
            String userName = null, userPwd = null;
            String userEmail = null;
            long reservID = 0;
            startTime = rs.getTimestamp("start_time");
            endTime = rs.getTimestamp("end_time");
            userName = rs.getString("user_name");
            userPwd = rs.getString("user_pwd");
            userEmail = rs.getString("user_email");
            reservID = rs.getLong("reserv_id");
            Reservation reserv = new Reservation(startTime, endTime, reservID, userName, userPwd, userEmail);
            return reserv;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.extractReservation : " + ex.getMessage());
            System.out.println("Exception in DBProxy.extractReservation : " + ex.getMessage());
            return null;
        }

    }

    public Reservation getNextReservation(){
        try{
            ResultSet rs = getNextReserv.executeQuery();
            Reservation reserv = extractReservation(rs);
            rs.close();
            return reserv;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.getNextReservation : " + ex.getMessage());
            System.out.println("Exception in DBProxy.getNextReservation : " + ex.getMessage());
        }
        return null;
    }

    public boolean delReservation(long reservID){
        try{
            delReserv.setLong(1, reservID);
            int rowCnt = delReserv.executeUpdate();
            if(rowCnt >= 1)
                return true;
            else
                return false;
        }
        catch(Exception ex){
            logger.write("Exception in DBProxy.delReservation : " + ex.getMessage());
            System.out.println("Exception in DBProxy.delReservation : " + ex.getMessage());
            return false;
        }
    }


    public static void main(String argv[]){
        DBProxy dbConn = new DBProxy(); 
        for(int i = 0; i < 20; ++i){
            Reservation reserv = Reservation.getRandReserv();
            System.out.println(reserv.serialize());
            System.out.println("Add Reservation: " + dbConn.addReservation(reserv));
        }
        ReservationList reservList = dbConn.getReservationList();
        System.out.println(reservList.serialize());
        System.out.println("Test next and delete");
        Reservation reserv = dbConn.getNextReservation();
        System.out.println(reserv.serialize());
        dbConn.delReservation(reserv.getReservID());
        reserv = dbConn.getNextReservation();
        System.out.println(reserv.serialize());
        dbConn.mvToHistory(reserv.getReservID());
        reserv = dbConn.getNextReservation();
        System.out.println(reserv.serialize());
    }
}
