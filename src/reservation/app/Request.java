package dcapi.app;

import dcapi.util.*;

public class Request{

    public static final String CMD_ADD = "add";
    public static final String CMD_GET = "get";

    private String cmd = null;
    private Reservation reserv = null;

    public Request(String cmd, Reservation reserv){
        this.cmd = cmd;
        if(null != reserv){
            this.reserv = new Reservation(reserv);
        }
    }

    public String getCMD(){
        return this.cmd;
    }

    public Reservation getReservation(){
        return this.reserv;
    }
}
