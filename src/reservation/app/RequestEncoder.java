package dcapi.app;

import dcapi.util.*;

import com.google.gson.Gson;

import java.util.*;
import java.util.regex.Pattern;


public class RequestEncoder{
    private ConfKeeper conf = null;
    private StrValidator strValid = null;

    private String prefix = null;

    public RequestEncoder(){
        this(new ConfKeeper());
    }

    public RequestEncoder(ConfKeeper conf){
        this.conf = conf;
        strValid = new StrValidator(this.conf);
        prefix = conf.getCCNPrefix();
    }

    public String encode(Request request){
        String cmd = request.getCMD();
        String strInterest = null;
        String reservation = null;
        //NOTE: the timestamp is essential if we do not want avoid getting data from cache
        long curTime = System.currentTimeMillis();
        if(0 == cmd.compareTo("get")){
            strInterest = prefix + curTime + "/" + cmd + "/";
        }
        else if(0 == cmd.compareTo("add")){
            reservation = strValid.toValid(request.getReservation().serialize());
            strInterest = prefix + curTime + "/" + cmd + "/" + reservation + "/";
        }
        else{
            System.out.println("Invalid cmd : " + cmd);
            return null;            
        }
        return strInterest;
    }

    public static void main(String argv[]){
        Reservation reserv = Reservation.getRandReserv();
        System.out.println(reserv.serialize());
        Request request = new Request("add", reserv);
        RequestEncoder enc = new RequestEncoder();
        String strInterest = enc.encode(request);
        System.out.println("strInterest is : " + strInterest);
        RequestDecoder dec = new RequestDecoder();
        Request request1 = dec.decode(strInterest);
        System.out.println("Request is : " + request1.getCMD() + ", " + request1.getReservation().serialize());
    }
}
