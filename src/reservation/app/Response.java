package dcapi.app;

import dcapi.util.*;
import com.google.gson.Gson;

public class Response{

    public static final int ADD_SUCCESS = 0;
    public static final int GET_SUCCESS = 1;
    public static final int INVALID_CMD = 2;
    public static final int INVALID_SLOT = 3;
    public static final int INVALID_START = 4;
    public static final int INVALID_USR = 5;
    public static final int INVALID_PWD = 6;
    public static final int INVALID_EMAIL = 7;
    public static final int CONFLICT = 8;
    public static final int CONFLICT_USR = 9;

    private int code = 0;
    private ReservationList reservList = null;

    public Response(int code){
        this.code = code;
    }

    public Response(int code, ReservationList reservList){
        this(code);
        this.reservList = new ReservationList(reservList);
    }

    public int getCode(){
        return this.code;
    }

    public String serialize(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static Response deSerialize(String data){
        try{
            Gson gson = new Gson();
            return gson.fromJson(data, Response.class);
        }
        catch(Exception ex){
            System.out.println("Exception in Response-deSerialize: invalid string");
            return null;
        }
    }

    public ReservationList getReservList(){
        return new ReservationList(this.reservList);
    }
}
