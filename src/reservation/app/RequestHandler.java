package dcapi.app;

import dcapi.util.*;
import dcapi.database.*;

import java.sql.Timestamp;

public class RequestHandler{

    private DBProxy dbProxy = null;
    private EmailProxy email = null;
    private ConfKeeper conf = null;

    public RequestHandler(String emailPWD){
        this(new ConfKeeper(), emailPWD);
    }

    public RequestHandler(ConfKeeper conf, String emailPWD){
        this.conf = conf;
        dbProxy = new DBProxy(this.conf);
        email = new EmailProxy(this.conf, emailPWD);
    }


    private void sendNotifyEmail(Reservation reserv){
        String msg = EmailWriter.getNotifyMsg(reserv);
        String title = EmailWriter.getNotifyTitle();
        email.sendEmail(reserv.getUserEmail(), title, msg);
    }

    public Response handle(Request request){
        String cmd = request.getCMD();
        Response response = null;
        if(cmd.compareTo("add") == 0){
            Reservation reserv = request.getReservation();

            int checkRes = Reservation.checkReservation(reserv);

            //check username conflicts
            if(conf.hasUser(reserv.getUserName())){
                response = new Response(Response.CONFLICT_USR);
            }
            else if(Reservation.VALID_RESERVATION == checkRes){
                reserv.setUserPWD(Reservation.getRandPWD());
                if(dbProxy.addReservation(reserv)){
                    response = new Response(Response.ADD_SUCCESS);
                    sendNotifyEmail(reserv);
                }
                else{
                    response = new Response(Response.CONFLICT);
                }
            }
            else if(Reservation.INVALID_USR == checkRes){
                response = new Response(Response.INVALID_USR);
            }
            else if(Reservation.INVALID_SLOT == checkRes){
                response = new Response(Response.INVALID_SLOT);
            }
            else if(Reservation.INVALID_START == checkRes){
                response = new Response(Response.INVALID_START);
            }
            else if(Reservation.INVALID_EMAIL == checkRes){
                response = new Response(Response.INVALID_EMAIL);
            }

        }
        else if(cmd.compareTo("get") == 0){
            ReservationList reservList = dbProxy.getReservationList();
            response = new Response(Response.GET_SUCCESS, reservList);
        }
        else{
            System.out.println("In RequestHandler-handle : get invalid cmd " + cmd);
            response = new Response(Response.INVALID_CMD);
        }   
        return response;
    }

/* email password removed
    public static void main(String argv[]){
        RequestDecoder dec = new RequestDecoder();
        //Request request = dec.decode("ccnx:/ndn/uiuc.edu/dcapi/add/"
        //        + "{\"start_time\":\"Mar 21, 2012 12:21:44 PM\",\"end_time\":\"Mar 22, 2012 12:17:38 AM\""
        //        + ",\"reserv_id\":-1,\"user_name\":\"qw54_8qb9v\",\"user_pwd\":\"p@.d79ky6j\",\"user_email\":\"mfeb5sl1va\"}");
        Request request = new Request("get", null);
        RequestEncoder enc = new RequestEncoder();
        request = dec.decode(enc.encode(request));
        RequestHandler reqHandler = new RequestHandler("email password");
        Response response = reqHandler.handle(request);
        System.out.println(response.serialize());
    } 
*/
}
