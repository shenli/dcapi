package dcapi.app;

import com.google.gson.Gson;
import dcapi.util.*;

public class RequestDecoder{

    private static final int TIME_IND = 0;
    private static final int CMD_IND = 1;
    private static final int RESERV_IND = 2;

    private ConfKeeper conf = null;
    private StrValidator strValid = null;

    private String prefix = null;

    public RequestDecoder(){
        this(new ConfKeeper());
    }

    public RequestDecoder(ConfKeeper conf){
        this.conf = conf;
        strValid = new StrValidator(this.conf);
        prefix = conf.getCCNPrefix();
    }

    public Request decode(String url){
        String suffix = url.replaceFirst(prefix, "");
        while('/' == suffix.charAt(0)){
            suffix = suffix.replaceFirst("/", "");
        }
        String [] items = suffix.split("/");
        String cmd = items[CMD_IND];
        Reservation reserv = null;
        //my version of java cannot do switch on String...
        if(0 == cmd.compareTo("get")){
        }
        else if(0 == cmd.compareTo("add")){
            
            reserv = Reservation.deSerialize(strValid.fromValid(items[RESERV_IND]));
        }
        else{
            System.out.println("Got an invalid request: " + suffix);
            return null;
        }       
        Request request = new Request(cmd, reserv);
        Gson gson = new Gson();
        System.out.println(gson.toJson(items));
        System.out.println(gson.toJson(request));
        return request;        
    }


    public static void main(String args[]){
        RequestDecoder dec = new RequestDecoder();
        dec.decode("ccnx:/ndn/uiuc.edu/dcapi/add/"
                + "{\"start_time\":\"Mar 21, 2012 12:21:44 PM\",\"end_time\":\"Mar 22, 2012 12:17:38 AM\"" 
                + ",\"reserv_id\":-1,\"user_name\":\"qw54_8qb9v\",\"user_pwd\":\"p@.d79ky6j\",\"user_email\":\"mfeb5sl1va\"}");
    }
}
