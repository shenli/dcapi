import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Command;

import java.io.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Exec {

    public static void main(String... args)
            throws IOException {
        final SSHClient ssh = new SSHClient();
//        ssh.loadKnownHosts(new File("/home/geni/.ssh/known_hosts"));
        ssh.addHostKeyVerifier("a4:21:47:d3:ae:7a:97:e5:6c:c2:49:2f:f8:fb:6f:1f");
        ssh.connect("localhost");
        try {
            ssh.authPublickey(System.getProperty("geni"));
            final Session session = ssh.startSession();
            try {
                final Command cmd = session.exec("ping -c 1 google.com");
                System.out.println(IOUtils.readFully(cmd.getInputStream()).toString());
                cmd.join(5, TimeUnit.SECONDS);
                System.out.println("\n** exit status: " + cmd.getExitStatus());
            } finally {
                session.close();
            }
        } finally {
            ssh.disconnect();
        }
    }

}
