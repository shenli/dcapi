from threading import Thread, Lock
import time
import re
import commands

class PDU:

    sleepLen = 1
    lock = None

    cmdGetPower = "../exe/avocent_measure.php %s %d"
    pduMapPath = "../conf/outlet_server_map"
    pduURL = 'tarek4234-ckt2%d-pdu.cs.illinois.edu'

    curPower = {}
    pduMap = {}
    hostInd = 2
    portInd = 1
    pduInd = 0

    tUpdate = None

    def __init__(self, sleepLen = 1):
        self.sleepLen = sleepLen
        self.lock = Lock()
        self._get_pdu_map()
        print self.pduMap

    def _get_pdu_map(self):
        f = open(self.pduMapPath, "r")
        
        for line in f:
            items = re.split(" +", line.split("\n")[0])
            self.pduMap[items[self.hostInd]] = (int(items[self.pduInd]), int(items[self.portInd]))

        f.close()
            

    def _get_one_power(self, host):
        pdu = self.pduURL%(self.pduMap[host][self.pduInd])
        port = self.pduMap[host][self.portInd]
        cmd = self.cmdGetPower%(pdu, port)
        #print cmd
        res = commands.getstatusoutput(cmd)
        return int(res[1])

    def _get_all_power(self):
        tmpPower = {}
        for host in self.pduMap.keys():
            tmpPower[host] = self._get_one_power(host)
        return tmpPower

    def _update(self):
        while self.startFlag:
            tmpPower = self._get_all_power()
            
            self.lock.acquire()
            self.curPower = tmpPower
            self.lock.release()

            time.sleep(self.sleepLen)

    def start(self):
        self.startFlag = True

        self.tUpdate = Thread(target = self._update)
        self.tUpdate.setDaemon(True)
        self.tUpdate.start()

    def stop(self, block = True):
        self.startFlag = False
        if block:
            self.tUpdate.join()

    def get_one_power(self, host):
        if host not in self.pduMap.keys():
            print "The valid hosts are ",
            print self.pduMap.keys()
            return -1
        self.lock.acquire()
        power = self.curPower[host]
        self.lock.release()
        return power

    def get_all_power(self):
        self.lock.acquire()
        powers = self.curPower.copy()
        self.lock.release()
        return powers


def main():
    pdu = PDU()
    pdu._get_one_power("tarekc01")
    pdu._get_all_power()

    pdu.start()
    time.sleep(10)
    print pdu.get_one_power("tarekc40")
    time.sleep(10)
    print pdu.get_one_power("tarekc40")
    print pdu.get_all_power()

if __name__ == "__main__":
    main()

