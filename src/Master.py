#!/usr/bin/python

from PDU import PDU
from CRAC import CRAC
from Util import Util
from SimpleXMLRPCServer import SimpleXMLRPCServer
from threading import Thread, Lock
import time
import xmlrpclib
import re, sys, os


class Master:

    sleepLen = 1
    usr = None
    pwd = None
    lock = None

    tHeartbeat = None
    tMaster = None    

    startFlag = False

    pdu = None
    crac = None
    util = None

    host = None
    rpcPorts = None
    masterAddr = None
    workers = None
    baseDir = None
    

    controlConn = None

    allSleepLen = None

    cmdInvokeWorker = "\"echo %s | /usr/bin/python %s/src/Worker.py &> %s/log/%s_worker.log &\""

    def __init__(self, pwd):
        self.util = Util()

        self.allSleepLen = self.util.get_sample_freq()
        self.host = self.util.get_host_name()
        self.usr = self.util.get_user_name()
        self.rpcPorts = self.util.get_rpc_ports()
        self.masterAddr = self.util.get_master_addr()
        self.workers = self.util.get_workers()
        self.baseDir = self.util.get_base_dir()

        if not self.masterAddr == self.host:
            print "Master instance should run on server %s only! \n Abort!"%(self.masterAddr)
            return -1


        self.sleepLen = self.allSleepLen["Master"]
        self.pwd = pwd

        self.pdu = PDU(sleepLen = self.allSleepLen["PDU"])
        self.crac = CRAC(pwd = self.pwd, sleepLen = self.allSleepLen["CRAC"])
       
        self._start_master_rpc()   
        self._invoke_workers()
        time.sleep(2)
        self._init_control_conn()
        self._start_workers() 
 

    def _invoke_workers(self):
        for worker in self.workers:
            cmd = "ssh %s@%s 'echo %s  | %s/src/WorkerInvoker.py' "%(self.usr, worker, self.pwd, self.baseDir)
            print cmd
            os.popen(cmd)
            #user_host = "%s@%s"%("shenli3", worker)
            #cmd = self.cmdInvokeWorker%(self.pwd, self.baseDir, self.baseDir, worker)
            #subprocess.call(["ssh", user_host, cmd])

    def _start_workers(self):
        for worker in self.controlConn.keys():
            self.controlConn[worker].start()


    def _stop_workers(self):
        for worker in self.controlConn.keys():
            self.controlConn[worker].stop()

    def _start_heartbeat(self):
        pass

    def _perform_heartbeat(self):
        pass

    def _init_control_conn(self):
        self.controlConn = {}
        for worker in self.workers:
            print "%s, "%(worker),
            print self.rpcPorts["controlPort"]
            self.controlConn[worker] = xmlrpclib.Server("http://%s:%d"%(worker, self.rpcPorts["controlPort"]))

    def _start_master_rpc(self):
        self.tMaster = Thread(target = self._init_master_rpc)
        self.tMaster.setDaemon(True)
        self.tMaster.start()

    def _init_master_rpc(self):
        SimpleXMLRPCServer.allow_reuse_address = 1
        server = SimpleXMLRPCServer((self.host, self.rpcPorts["masterPort"]));
        server.register_introspection_functions()

        server.register_function(self.get_all_power, "get_all_power")
        server.register_function(self.get_crac_target, "get_crac_target")
        server.register_function(self.get_crac_in_temp, "get_crac_in_temp")
        server.register_function(self.get_crac_out_temp, "get_crac_out_temp")

        server.serve_forever()

    def start(self):
        self.startFlag = True
        self.pdu.start()
        self.crac.start()

    def stop(self, block = True):
        self.startFlag = False
        
        self.pdu.stop(block)
        self.crac.stop(block)


    def get_all_power(self):
        return self.pdu.get_all_power()

    def get_crac_target(self):
        return self.crac.get_target()

    def get_crac_in_temp(self):
        return self.crac.get_in_temp()

    def get_crac_out_temp(self):
        return self.crac.get_out_temp()


        

def main():
    pwd = sys.stdin.readline()
    pwd = pwd.split("\n")[0]

    master = Master(pwd)
    master.start()
    time.sleep(5)
    print master.get_all_power()
    print master.get_crac_target()
    print master.get_crac_in_temp()
    print master.get_crac_out_temp()

    cmd = sys.stdin.readline()


if __name__ == "__main__":
    main()








