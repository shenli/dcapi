import re, os, sys

class Util:

    baseDir = None
    masterAddrPath = "/conf/master"
    rpcPortPath = "/conf/rpc_conf"
    samplePath = "/conf/sample_conf"
    workersPath = "/conf/workers"


    def __init__(self):
        self.baseDir = self._get_base_dir()
        
        self.masterAddrPath = "%s%s"%(self.baseDir, self.masterAddrPath)
        self.rpcPortPath = "%s%s"%(self.baseDir, self.rpcPortPath)
        self.samplePath = "%s%s"%(self.baseDir, self.samplePath)
        self.workersPath = "%s%s"%(self.baseDir, self.workersPath)

    def _get_base_dir(self):
        return (os.environ["CLUSTER_API_HOME"])
        
    def get_base_dir(self):
        return self.baseDir

    def get_sample_freq(self):
        tmpSleepLen = {}
        f = open(self.samplePath, "r")
        for line in f:
            items = re.split(" +", line)
            tmpSleepLen[items[0]] = float(items[1])
        return tmpSleepLen

    def get_master_addr(self):
        f = open(self.masterAddrPath, "r")
        master = f.readline()
        master = master.split("\n")[0]
        return master

    def get_rpc_ports(self):
        tmpRpcPorts = {}
        f = open(self.rpcPortPath, "r")
        for line in f:
            items = re.split(" +", line)
            tmpRpcPorts[items[0]] = int(items[1])

        return tmpRpcPorts

    def get_host_name(self):
        f = os.popen('hostname')
        l = f.readlines()
        name = l[0].split('.')[0]
        f.close()
        return name

    def get_user_name(self):
        f = os.popen('who')
        l = f.readline()
        name = re.split(" +", l)[0]
        return name

    def get_workers(self):
        workers = []
        f = open(self.workersPath, "r")
        for line in f:
            workers.append(line.split("\n")[0])
        return workers

def main():
    util = Util()
    print util.get_master_addr()
    print util.get_rpc_ports()
    print util.get_host_name()
    print "Workers"
    print util.get_workers()
    print "%s"%(util.get_base_dir())
    print util.get_user_name()

if __name__ == "__main__":
    main()
