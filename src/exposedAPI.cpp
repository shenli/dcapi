#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>

#define MAX_CMD_LEN 200

int main(int argc, char * argv[]){
    char * baseDir;
    char * apiPath = "/src/HiddenAPI.py";
    char cmd[MAX_CMD_LEN];
    baseDir = getenv("CLUSTER_API_HOME");
    strcpy(cmd, baseDir);
    strcat(cmd, apiPath);
    int i = 0;
    while(++i < argc){
        strcat(cmd, " ");
        strcat(cmd, argv[i]);
    }
    
    //printf("%s\n", cmd);
    system(cmd);
}
