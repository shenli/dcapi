from threading import Thread
from threading import Lock
import time
import commands
import re

class Memory:

    #sensing interval = 1s
    sleepLen = 1

    #update and read lock
    lock = None 

    #accumulated interruption
    memIN = 0
    #accumulated context switch
    memCS = 0
    #interruption
    curMemIN = 0
    #context switch
    curMemCS = 0
    #memory utilization: notice that Linux does a lot of caching, the memUtil may not be that useful
    curMemUtil = 0

    #update thread
    tUpdate = None
    startFlag = False

    cmdGetMem = "vmstat -s | tail -n 4 | head -n 2"
    cmdGetUtil = "free | head -n 2 | tail -n 1"
    def __init__(self, sleepLen = 1):
        self.sleepLen = sleepLen
        (self.memIN, self.memCS) = self._get_cur_memory()
        self.lock = Lock()
#        self.start()


    def _get_cur_memory(self):
        res = commands.getstatusoutput(self.cmdGetMem)
        lines = res[len(res) - 1].split('\n')
        interrupt = re.split(' +', lines[0])
        cs = re.split(' +', lines[1])
        return (int(interrupt[1]), int(cs[1]))


    def _get_cur_util(self):
        res = commands.getstatusoutput(self.cmdGetUtil)
        items = re.split(' +', res[len(res) - 1])
        return float(items[2])/float(items[1])


    def _update(self):
        while self.startFlag:
            cur = self._get_cur_memory()
            tmpUtil = self._get_cur_util()
            
            self.lock.acquire()
            self.curMemIN = cur[0] - self.memIN
            self.curMemCS = cur[1] - self.memCS
            self.memIN = cur[0]
            self.memCS = cur[1]
            self.curMemUtil = tmpUtil
            self.lock.release()
            time.sleep(self.sleepLen)

    
    def start(self):
        self.startFlag = True
        self.tUpdate = Thread(target=self._update)
        self.tUpdate.setDaemon(True)
        self.tUpdate.start()

    def stop(self, block = True):
        self.stopFlag = False
        if block:
            self.tUpdate.join()


    def get_util(self):
        if self.startFlag == False:
            return -1
        self.lock.acquire()
        tmpUtil = self.curMemUtil
        self.lock.release()
        return tmpUtil

    def get_CS(self):
        if self.startFlag == False:
            return -1
        self.lock.acquire()
        tmpCS = self.curMemCS
        self.lock.release()
        return tmpCS

    def get_IN(self):
        if self.startFlag == False:
            return -1
        self.lock.acquire()
        tmpIN = self.curMemIN
        self.lock.release()
        return tmpIN



def main():
    mem = Memory()
    mem.start()
    time.sleep(2)
    print mem.get_util()
    print mem.get_CS()
    print mem.get_IN()
    

if __name__ == "__main__":
    main()
