#!/usr/bin/python

from Memory import Memory
from CPU import CPU
from NIC import NIC
from Util import Util
from SimpleXMLRPCServer import SimpleXMLRPCServer
from threading import Thread, Lock
import time
import xmlrpclib
import re, sys, os

class Worker:

    sleepLen = 1
    lock = None
    tUpdate = None
    tWorker = None

    startFlag = False

    cpu = None
    nic = None
    mem = None
    util = None
    masterConn = None
    #TODO: heartbeat mesages
    master = None

    curPowers = None
    curCracTarget = 0
    curCracIn = 0
    curCracOut = 0

    pwd = None
    host = None
    baseDir = None
    rpcPorts = None
    masterAddr = None

    def __init__(self, pwd, sleepLen = 1):
        self.sleepLen = sleepLen
        self.pwd = pwd

        self.lock = Lock()
        self.cpu = CPU(self.pwd)
        self.nic = NIC()
        self.mem = Memory()
        self.util = Util()        

        self.host = self.util.get_host_name()
        self.masterAddr = self.util.get_master_addr()
        self.rpcPorts = self.util.get_rpc_ports()

    def _init_master_conn(self):
        url = "http://%s:%d"%(self.masterAddr, self.rpcPorts["masterPort"])
        print url
        self.masterConn = xmlrpclib.Server(url)

    def _start_control_rpc(self):
        SimpleXMLRPCServer.allow_reuse_address = 1
        print "%s, "%(self.host),
        print self.rpcPorts["controlPort"]
        server = SimpleXMLRPCServer((self.host, self.rpcPorts["controlPort"]));
        server.register_introspection_functions()
        
        server.register_function(self.start, "start")
        server.register_function(self.stop, "stop")
        server.register_function(self.heartbeat, "heartbeat")

        server.serve_forever()        
        
    def heartbeat(self):
        return 0

    def _start_worker_rpc(self):
        self.tWorker = Thread(target = self._init_worker_rpc)
        self.tWorker.setDaemon(True)
        self.tWorker.start()

    def _init_worker_rpc(self):
        SimpleXMLRPCServer.allow_reuse_address = 1
        server = SimpleXMLRPCServer((self.host, self.rpcPorts["workerPort"]));
        server.register_introspection_functions()
        
        server.register_function(self.get_cpu_temp, "get_cpu_temp")
        server.register_function(self.get_cpu_util, "get_cpu_util")
        server.register_function(self.get_cpu_freq, "get_cpu_freq")
        server.register_function(self.set_cpu_freq, "set_cpu_freq")
        server.register_function(self.get_mem_util, "get_mem_util")
        server.register_function(self.get_mem_CS, "get_mem_CS")
        server.register_function(self.get_mem_IN, "get_mem_IN")
        server.register_function(self.get_net, "get_net")
        server.register_function(self.get_self_power, "get_self_power")
        server.register_function(self.get_all_power, "get_all_power")
        server.register_function(self.get_crac_target, "get_crac_target")
        server.register_function(self.get_crac_in_temp, "get_crac_in_temp")
        server.register_function(self.get_crac_out_temp, "get_crac_out_temp")
        server.register_function(self.check_start, "check_start")

        server.serve_forever()

    def _get_all_power(self):
        return self.masterConn.get_all_power()

    # computer room air conditioning APIs

    def _get_crac_target(self):
        return self.masterConn.get_crac_target()

    def _get_crac_in_temp(self):
        return self.masterConn.get_crac_in_temp()

    def _get_crac_out_temp(self):
        return self.masterConn.get_crac_out_temp()

    def _update(self):
        while self.startFlag:
            tmpPowers = self._get_all_power()
            tmpCracTarget = self._get_crac_target()
            tmpCracIn = self._get_crac_in_temp()
            tmpCracOut = self._get_crac_out_temp()

            self.lock.acquire()
            self.curPowers = tmpPowers
            self.curCracTarget = tmpCracTarget
            self.curCracIn = tmpCracIn
            self.curCracOut = tmpCracOut
            self.lock.release()

            time.sleep(self.sleepLen)

    def start(self):
        self._init_master_conn()
        self.cpu.start()
        self.mem.start()
        self.nic.start()
        self.startFlag = True
        self.tUpdate = Thread(target = self._update)
        self.tUpdate.setDaemon(True)
        self.tUpdate.start()
        self._start_worker_rpc()
        return 0

    def stop(self, block = True):
        self.startFlag = False
        if block:
            self.tUpdate.join()
        self.cpu.stop(block)
        self.mem.stop(block)
        self.nic.stop(block)
        return 0

    def serve(self):
        self._start_control_rpc()

    def check_start(self):
        if not self.startFlag:
            print "worker instance is not started yet!"
            return False
        else:
            return True

    # CPU APIs
    def get_cpu_temp(self):
        return self.cpu.get_temp()

    def get_cpu_util(self):
        return self.cpu.get_util()

    def get_cpu_freq(self):
        return self.cpu.get_freq()

    def set_cpu_freq(self, freq):
        self.cpu.set_freq(freq)
        return 0

    # memory APIs
    def get_mem_util(self):
        return self.mem.get_util()

    def get_mem_CS(self):
        return self.mem.get_CS()

    def get_mem_IN(self):
        return self.mem.get_IN()


    # Network APIs
    def get_net(self):
        return self.nic.get_net()

    # Power distribution units APIs

    def get_self_power(self):
        if not self.check_start():
            return -1
    
        self.lock.acquire()
        tmpSelfPower = self.curPowers[self.host]
        self.lock.release()
        
        return tmpSelfPower

    def get_all_power(self):
        if not self.check_start():
            return -1

        self.lock.acquire()
        tmpAllPower = self.curPowers.copy()
        self.lock.release()

        return tmpAllPower

    # computer room air conditioning APIs

    def get_crac_target(self):
        if not self.check_start():
            return -1

        self.lock.acquire()
        tmpCracTarget = self.curCracTarget
        self.lock.release()

        return tmpCracTarget

    def get_crac_in_temp(self):
        if not self.check_start():
            return -1

        self.lock.acquire()
        tmpCracIn = self.curCracIn
        self.lock.release()

        return tmpCracIn

    def get_crac_out_temp(self):
        if not self.check_start():
            return -1

        self.lock.acquire()
        tmpCracOut = self.curCracOut
        self.lock.release()

        return tmpCracOut


def test():
    pwd = sys.stdin.readline()
    pwd = pwd.split("\n")[0]

    worker = Worker(pwd)

    worker.start()
    print "started"
    time.sleep(5)
    print worker.get_all_power()
    print worker.get_self_power()
    print worker.get_net()
    print worker.get_crac_out_temp()
    print worker.get_crac_in_temp()
    print worker.get_crac_target()
    print worker.get_cpu_freq()
    worker.set_cpu_freq(1995000)
    time.sleep(5)
    print worker.get_cpu_freq()
    print worker.get_cpu_util()
    print worker.get_cpu_temp()
    print worker.get_mem_util()
    print worker.get_mem_IN()
    print worker.get_mem_CS()
   
    cmd = sys.stdin.readline() 


def main():
    pwd = sys.stdin.readline()
    pwd = pwd.split("\n")[0]

    print "Got Password!"
    sys.stdout.flush()

    worker = Worker(pwd)
    print "Worker Constructed!"
    sys.stdout.flush()
    worker.serve()    


if __name__ == "__main__":
    main()

