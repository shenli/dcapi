from threading import Thread, Lock
import time
import commands
import re

class NIC:

    sleepLen = 1
    lock = None
    startFlag = False

    tUpdate = None

    recByteInd = 0
    recPacketInd = 1
    sendByteInd = 8
    sendPacketInd = 9

    accRecByte = 0
    accRecPacket = 0
    accSendByte = 0
    accSendPacket = 0

    curRecByte = 0
    curRecPacket = 0
    curSendByte = 0
    curSendPacket = 0

    #TODO: remove head command
    cmdFaceList = {"lo":3, "eth0":4}
    cmdGetNet = "cat /proc/net/dev | head -n %d | tail -n 1"

    def __init__(self, sleepLen = 1):
        self.sleepLen = sleepLen
        (self.accRecByte, self.accRecPacket, self.accSendByte, self.accSendPacket) = self._get_cur_net()
        self.lock = Lock()

    def _get_cur_net(self, face = "eth0"):
        res = commands.getstatusoutput(self.cmdGetNet%(self.cmdFaceList[face]))
        line = (res[1].split(":"))[1]
        items = re.split(" +", line)
        return (int(items[self.recByteInd]), int(items[self.recPacketInd]), int(items[self.sendByteInd]), int(items[self.sendPacketInd]))

    def _update(self):
        while self.startFlag:
            (tmpRecByte, tmpRecPacket, tmpSendByte, tmpSendPacket) = self._get_cur_net()
            
            self.lock.acquire()
            self.curRecByte = tmpRecByte - self.accRecByte
            self.curRecPacket = tmpRecPacket - self.accRecPacket
            self.curSendByte = tmpSendByte - self.accSendByte
            self.curSendPacket = tmpSendPacket - self.accSendPacket
            
            self.accRecByte = tmpRecByte
            self.accRecPacket = tmpRecPacket
            self.accSendByte = tmpSendByte
            self.accSendPacket = tmpSendPacket
            self.lock.release()

            time.sleep(self.sleepLen)

    def start(self):
        self.startFlag = True
        self.tUpdate = Thread(target = self._update)
        self.tUpdate.setDaemon(True)
        self.tUpdate.start()

    def stop(self, block = True):
        self.startFlag = False
        if block:
            self.tUpdate.join()

    def get_net(self):
        if not self.startFlag:
            return -1
        self.lock.acquire()
        tmpRecByte = self.curRecByte
        tmpRecPacket = self.curRecPacket
        tmpSendByte = self.curSendByte
        tmpSendPacket = self.curSendPacket
        self.lock.release()
        return (tmpRecByte, tmpRecPacket, tmpSendByte, tmpSendPacket)


def main():
    nic = NIC()
    print nic._get_cur_net()
    nic.start()
    print nic.get_net()
    time.sleep(2)
    print nic.get_net()

if __name__ == "__main__":
    main()
