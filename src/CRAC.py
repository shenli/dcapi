from threading import Thread, Lock
import time
import commands
import re
import sys
import os

class CRAC:

    sleepLen = 30
    tUpdate = None
    startFlag = False
    lock = Lock()
    pwd = None

    phyInPort = 5
    phyOutPort = 6
    logInPort = 0
    logOutPort = 0

    curTarget = 0
    curIn = 0
    curOut = 0

    cmdGetCRAC = "echo %s | sudo -S ../exe/read_CRAC | tail -n 2 | head -n 1"
    cmdSetCRAC = "echo %s | sudo -S ../exe/set_CRAC %d"
    cmdGetTemp = "echo %s | sudo -S ../exe/thermal_sensor_api %d"
    

    def __init__(self, pwd, sleepLen = 3):
        self.sleepLen = sleepLen
        self.pwd = pwd
        self.logInPort = self._get_dev_num(self.phyInPort)
        self.logOutPort = self._get_dev_num(self.phyOutPort)
        self.lock = Lock()        

    def _get_dev_num(self, portNum):
        f = os.popen('find /sys -name dev | grep 2-1.2.%d:1.1'%(portNum))
        line = (f.readlines())[0]
        line = (line.split('usbdev2.'))[1]
        line = (line.split('_ep82'))[0]
        if len(line) < 1:
            print 'No Device Found on port %d!'%(portNum)
        return int(line)



    def _get_cur_target(self):
        res = commands.getstatusoutput(self.cmdGetCRAC%(self.pwd))
        #print res
        #print len(res)
        res = res[1].split("\n")
        #print res
        return int(res[len(res) - 1])

    def _set_target(self, target):
        commands.getstatusoutput(self.cmdSetCRAC%(self.pwd, target))
        self.lock.acquire()
        self.curTarget = target
        self.lock.release()

    def _get_cur_temp(self, port):
        res = commands.getstatusoutput(self.cmdGetTemp%(self.pwd, port))
        res = res[1].split("\n")
        return float(res[len(res) - 1])

    def _update(self):
        while self.startFlag:
            tmpIn = self._get_cur_temp(self.logInPort)
            tmpOut = self._get_cur_temp(self.logOutPort)
            tmpTarget = self._get_cur_target()

            self.lock.acquire()
            self.curTarget = tmpTarget
            self.curIn = tmpIn
            self.curOut = tmpOut
            self.lock.release()

            time.sleep(self.sleepLen)

    def start(self):
        self.startFlag = True
        self.tUpdate = Thread(target = self._update)
        self.tUpdate.setDaemon(True)
        self.tUpdate.start()

    def stop(self, block = True):
        self.startFlag = False
        if block:
            self.tUpdate.join()

    def get_target(self):
        if not self.startFlag:
            print "CRAC component not started!"
            return -1

        self.lock.acquire()
        tmpTarget = self.curTarget
        self.lock.release()
        return tmpTarget

    def set_target(self):
        print "Set CRAC API is not revealed"
        return -1
        if not self.startFlag:
            print "CRAC component not started!"
            return -1

    def get_in_temp(self):
        if not self.startFlag:
            print "CRAC component not started!"
            return -1

        self.lock.acquire()
        tmpIn = self.curIn
        self.lock.release()
        return tmpIn

    def get_out_temp(self):
        if not self.startFlag:
            print "CRAC component not started!"
            return -1

        self.lock.acquire()
        tmpOut = self.curOut
        self.lock.release()
        return tmpOut


def main():
    pwd = sys.stdin.readline()
    pwd = pwd.split("\n")[0]
    crac = CRAC(pwd)
    print crac._get_cur_target()

    crac._get_cur_temp(crac.logInPort)
    crac._get_cur_temp(crac.logOutPort)

    crac.start()
    print crac.get_target()
    print crac.get_in_temp()
    print crac.get_out_temp()
    crac._set_target(50)
    time.sleep(5)
    print crac.get_target()
    print crac.get_in_temp()
    print crac.get_out_temp()
    time.sleep(10)
    crac.stop()  
 

if __name__ == "__main__":
    main()
