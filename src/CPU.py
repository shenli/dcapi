# -*- coding: utf-8 -*-

from threading import Thread, Lock
import commands
import re
import os
import time
import sys

class CPU:

    sleepLen = 1
    lock = None
    pwd = None

    curTemp = 0
    curUtil = 0
    curFreq = 0

    accBusy = 0
    accTotal = 0
    
    curBusy = 0
    curTotal = 0

    startFlag = False

    tUpdate = None

    # I should stop using head commands, since it will raise an "broken pipe" error if more lines passed then head command desired 
    freqLineNo = 7
    freqPath = "/proc/cpuinfo"
    utilPath = "/proc/stat"
    cmdSetFreq = "echo %s | sudo -S sh -c '\
                    echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor;\
                    echo userspace > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor;\
                    echo userspace > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor;\
                    echo userspace > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor;\
                    echo %d > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed;\
                    echo %d > /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed;\
                    echo %d > /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed;\
                    echo %d > /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed'"

    freqList = [2395000, 2394000, 2261000, 2128000, 1995000, 1862000, 1729000, 1596000, 1463000, 1330000, 1197000]

    def __init__(self, pwd, sleepLen = 1):
        self.pwd = pwd
        self.sleepLen = sleepLen
        self.lock = Lock()
        (self.accBusy, self.accTotal) = self._get_cur_util()
#        self.start()

    def _set_freq(self, newFreq):
        commands.getstatusoutput(self.cmdSetFreq%(self.pwd, newFreq, newFreq, newFreq, newFreq))
        self.cpuFreq = newFreq

    def _get_cur_util(self):
        f = open(self.utilPath, "r")
        res = f.readline()
        f.close()
        cpuTimes = re.split(' +', res)
        busy = 0
        total = 0
        for i in range(1, 4):
            busy = busy + int(cpuTimes[i])
        lenth = len(cpuTimes)
        total = busy
        for i in range(4, lenth):
            total = total + int(cpuTimes[i])
        return (busy, total)


    def _get_cur_temp(self):
        f = os.popen('sensors')
        lines = f.readlines()
        if len(lines) < 4:
            print "does not support CPU temperature sensor!"
            return -2
        temps = []
        for line in lines:
            wlist = line.split()
            if len(wlist) > 0 and wlist[0] == "Core":
                wlist = wlist[2].split("°")
                temps.append(float(wlist[0]))

        f.close()
        resT =  (temps[0] + temps[1] + temps[2] + temps[3])/4.0
        return resT

    def _get_line(self, f, lineNo):
        for i in range(lineNo):
            line = f.readline()
        return line

    def _get_cur_freq(self):
        f = open(self.freqPath, "r")
        res =  self._get_line(f, self.freqLineNo)
        f.close()
        freq = re.split(" +", res)
        return float(freq[len(freq) - 1])


    def _update(self):
        while self.startFlag:
            (tmpBusy, tmpTotal) = self._get_cur_util()
            tmpFreq = self._get_cur_freq()
            tmpTemp = self._get_cur_temp()

            self.lock.acquire()
            #Utilization
            self.curBusy = tmpBusy - self.accBusy
            self.curTotal = tmpTotal - self.accTotal
            self.accBusy = tmpBusy
            self.accTotal = tmpTotal
            if 0 == self.curTotal:
                self.curUtil = 0
            else:
                self.curUtil = float(self.curBusy)/float(self.curTotal)
            #Frequency
            self.curFreq = tmpFreq
            #Temperature 
            self.curTemp = tmpTemp
            self.lock.release()

            time.sleep(self.sleepLen)
            

    def start(self):
        self.startFlag = True
        self.tUpdate = Thread(target = self._update)
        self.tUpdate.setDaemon(True)
        self.tUpdate.start()

    def stop(self, block = True):
        self.startFlag = False
        if block:
            self.tUpdate.join()


    def get_temp(self):
        if not self.startFlag:
            print "CPU component not started!"
            return -1
        self.lock.acquire()
        tmpTemp =  self.curTemp
        self.lock.release()
        return tmpTemp

    def get_util(self):
        if not self.startFlag:
            print "CPU component not started!"
            return -1
        self.lock.acquire()
        tmpUtil = self.curUtil
        self.lock.release()
        return tmpUtil

    def get_freq(self):
        if not self.startFlag:
            print "CPU component not started!"
            return -1
        self.lock.acquire()
        tmpFreq = self.curFreq
        self.lock.release()
        return tmpFreq

    def set_freq(self, newFreq):
        if newFreq not in self.freqList:
            print "The frequency has to be one of ",
            print self.freqList

        self._set_freq(int(newFreq))


def main():
    pwd = sys.stdin.readline()
    pwd = pwd.split("\n")[0]
    cpu = CPU(pwd)
    print "333",
    print cpu._get_cur_freq()
    cpu.start()
    time.sleep(2)
    cpu.set_freq(2395000)
    print cpu.get_util()
    time.sleep(2)
    print "444",
    print cpu.get_freq()
    print cpu.get_util()
    cpu.stop()

if __name__ == "__main__":
    main()

