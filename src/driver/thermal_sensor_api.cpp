#include <iostream>
#include <usb.h>
#include <cstdio>
#include <cstring>

using namespace std;

#define IDVENDOR	4400
#define IDPRODUCT	26124

struct usb_device * find_dev(int dev_num, usb_bus * bus, u_int16_t vendor, u_int16_t product){
	struct usb_device * dev;
        for(;bus;bus = bus->next){
                for(dev = bus->devices; dev; dev = dev->next){
                        if(dev->devnum == dev_num && dev->descriptor.idVendor == vendor && dev->descriptor.idProduct == product){
                                cout << "Device Found!\n";
				return dev;
                        }
                }
        }
	//cout<<"Cannot find device!\n";
	return NULL;
}


int send_to_device(int no, char * buf, usb_dev_handle * dev_handle){
        memset(buf, 0, sizeof(char) * 0x110);
        if(no == 1){
                buf[0] = 0x0A;  buf[1] = 0x0B;
                buf[2] = 0x0C;  buf[3] = 0x0D;
                buf[6] = 0x02;

                return usb_control_msg( dev_handle,   // device handler
                                        0x21,         //request type
                                        0x09,         //request
                                        0x200,        //mValue, report tyep: 02, report ID: 00
                                        0x01,         //index
                                        buf,          //data
                                        0x20,         //setup packet size
                                        0);           //timeout: 0 means unlimited
        }
        else if(no == 2){
                buf[0] = 0x54;
                return usb_control_msg( dev_handle,   // device handler
                                        0x21,         //request type
                                        0x09,         //request
                                        0x200,        //mValue, report tyep: 02, report ID: 00
                                        0x01,         //index
                                        buf,          //data
                                        0x20,         //setup packet size
                                        0);           //timeout: 0 means unlimited
        }
	else if(no == 3){
                buf[0] = 0x54;
                return usb_control_msg( dev_handle,   // device handler
                                        0xA1,         //request type
                                        0x01,         //request
                                        0x0300,       //mValue, report tyep: 02, report ID: 00
                                        0x0001,       //index
                                        buf,          //data
                                        0x100,        //data size
                                        0);           //timeout: 0 means unlimited
        }
        else if(no == 4){
                buf[0] = 0x0A;  buf[1] = 0x0B;
                buf[2] = 0x0C;  buf[3] = 0x0D;
                buf[6] = 0x01;
        }
        else{
                cout << "Wrong out packet ID!\n";
                return -1;
        }
}


int protocol_get(char * buf, usb_dev_handle * dev_handle){
        //send 0A 0B 0C 0D 00 00 01 00 ...
        buf[6] = 0x99;
        send_to_device(1, buf, dev_handle);

        //send 54 00 00 ...
        send_to_device(2, buf, dev_handle);

        //retrieve data
        int r = send_to_device(3, buf, dev_handle);

        return r;
}

float get_dec_temp(char * buf){
        int hi = (int)buf[0];
        int lo = (int)buf[1];
        int res = hi * 256 + lo;

        return 0.3922 * res / 100 - 0.1294;
}

	

int main(int argc, char * argv[]) {
      	struct usb_bus * bus;
		
	//initialization
	usb_init();
	usb_find_busses();
	usb_find_devices();
	bus = usb_get_busses();

	//enumerate devices
	struct usb_device * dev;
	int ret = 0;
	int dev_num = atoi(argv[argc-1]);	

	//find device
	dev = find_dev(dev_num, bus, IDVENDOR, IDPRODUCT);
	
	if(dev == NULL){
		//cout<<"cannot find device!"<<endl;
		return -1;
	}
	//open device
	usb_dev_handle * handle =  usb_open(dev);
	//detach interface
	if((ret = usb_detach_kernel_driver_np(handle, 1)) == 0){
		//cout<<"device detached!"<<endl;
	}
	else{
		//cout<<"detach result is : " << ret<<endl;
		//cout<<"No need to detach device!"<<endl;
	}

	//claim device
	if((ret = usb_claim_interface(handle, 1)) == 0){
		//cout<<"Claim Success!"<<endl;
	}
	else{
		//cout<<"Claim result is : "<<ret<<endl;
		//cout<<"Claim failed!"<<endl;
		return -2;
	}


	char * buf = new char[0x110];

	if(protocol_get(buf, handle) < 0){
            //communication error
            return -3;
        }
	
	float res = get_dec_temp(buf);

	cout<< res<<endl;



	delete[] buf;
	usb_release_interface(handle, 1);	
	usb_close(handle);
	
	return 0;
}

