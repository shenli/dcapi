/*
 * Copyright © 2001-2008 Stéphane Raimbault <stephane.raimbault@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <modbus/modbus.h>

/* The goal of this program is to check all major functions of
libmodbus:
- force_single_coil
- read_coil_status
- force_multiple_coils
- preset_single_register
- read_holding_registers
- preset_multiple_registers
- read_holding_registers

All these functions are called with random values on a address
range defined by the following defines. 
 */
#define LOOP		1
#define SLAVE		0x03
#define ADDRESS_START	0
#define ADDRESS_END	99
#define SETPOINT_W	349
#define SETPOINT_R	9
#define CURTEMP_R	0

/* 
 * Settting CRAC temperature setpoint.
 *
 * At each loop, the program works in the range ADDRESS_START to
 * ADDRESS_END then ADDRESS_START + 1 to ADDRESS_END and so on.
 */
int set_setpoint(int setPoint){
    int ret;
    int nb;
    uint16_t *tab_rq_registers;
    modbus_param_t mb_param;

    if(setPoint > 90 || setPoint < 40){
        printf("ERROR: SetPoint should located between 40F and 90F");
        return -1;
    }

    /* RTU parity : none, even, odd */
    modbus_init_rtu(&mb_param, "/dev/ttyUSB0", 9600, "none", 8, 1);

    modbus_set_debug(&mb_param, TRUE);
    if (modbus_connect(&mb_param) == -1) {
        printf("ERROR: Connection failed\n");
        return -2;
    }

    /* Allocate and initialize the different memory spaces */
    nb = ADDRESS_END - ADDRESS_START;

    tab_rq_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
    memset(tab_rq_registers, 0, nb * sizeof(uint16_t));

    //write setpoint
    tab_rq_registers[0] = (uint16_t)setPoint;
    ret = preset_single_register(&mb_param, SLAVE, SETPOINT_W, tab_rq_registers[0]);

    //free resources	
    free(tab_rq_registers);

    //Close the connection 
    modbus_close(&mb_param);


    if (ret != 1) {
        printf("ERROR: write to setpoint. ret = (%d)\n", ret);
        printf("Slave = %d, address = %d\n", SLAVE, SETPOINT_W);
        printf("Write to setpoint FAILED!\n");
        return -3;
    }
    else {
        //printf("Write to setpoint SUCCESS!\n");
        printf("The temperature is %d\n", tab_rq_registers[0]);
        return 0;
    }
}

/* Reader methods. */
int read_setpoint(){
    int setpoint = read_data(SETPOINT_R);
    return setpoint;
}

int read_temperature(){
    int temperature = read_data(CURTEMP_R);
    return temperature;
}

/* Read the data according to the register address provided */
int read_data(int addr){

    int ret;
    int nb;
    uint16_t *tab_rp_registers;
    modbus_param_t mb_param;

    /* RTU parity : none, even, odd */
    modbus_init_rtu(&mb_param, "/dev/ttyUSB0", 9600, "none", 8, 1);

    /* TCP */
    modbus_set_debug(&mb_param, TRUE);
    if (modbus_connect(&mb_param) == -1) {
        printf("ERROR: Connection failed\n");
        return -1;
    }

    /* Allocate and initialize the different memory spaces */
    nb = ADDRESS_END - ADDRESS_START;
    tab_rp_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
    memset(tab_rp_registers, 0, nb * sizeof(uint16_t));

    //printf("in read 1\n");
    //read setpoint
    ret = read_holding_registers(&mb_param, SLAVE, addr, 1, tab_rp_registers);
    //printf("read finished \n");
    /* Free the memory */
    free(tab_rp_registers);

    /* Close the connection */
    modbus_close(&mb_param);

    if (ret != 1) {
        printf("ERROR: read. ret = (%d)\n", ret);
        printf("Slave = %d, address = %d\n", SLAVE, SETPOINT_R);
        printf("read FAILED!\n");
        return -2;
    }
    else {
        //     printf("Current setpoint is %d\n", tab_rp_registers[0]);
        //printf("read SUCCESS!\n");
        return (int)tab_rp_registers[0];
    }
}


int main(void)
{
    int setPoint, curTemp;
    setPoint = read_setpoint();  
    curTemp = read_temperature(); 
    printf("\n%d\n%d\n", setPoint, curTemp);
    return 0;
}













