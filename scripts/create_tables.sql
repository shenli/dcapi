drop database dcapi;
create database dcapi;
use dcapi;

CREATE TABLE reservation(
    -- unique reservation id
    reserv_id BIGINT unsigned NOT NULL auto_increment,
    -- user name: used for login
    user_name varchar(64) binary NOT NULL default 'user', 
    -- user password: used for login, will be different for different reservation
    user_pwd varchar(32) binary NOT NULL default '123456',
    -- user email: used for notifying password
    user_email varchar(64) binary NOT NULL default 'user@uiuc_cluster',
    -- start time
    start_time timestamp,
    -- end time
    end_time timestamp,


    PRIMARY KEY reserv_id (reserv_id),
    INDEX start_time (start_time),
    INDEX end_time (end_time)

) ENGINE=InnoDB CHARACTER SET=binary;


CREATE TABLE history(
    -- unique reservation id
    reserv_id BIGINT unsigned NOT NULL,
    -- user name: used for login
    user_name varchar(64) binary NOT NULL default 'user',
    -- user password: used for login, will be different for different reservation
    user_pwd varchar(32) binary NOT NULL default '123456',
    -- user email: used for notifying password
    user_email varchar(64) binary NOT NULL default 'user@uiuc_cluster',
    -- start time
    start_time timestamp,
    -- end time
    end_time timestamp,


    PRIMARY KEY reserv_id (reserv_id),
    INDEX start_time (start_time),
    INDEX end_time (end_time)

) ENGINE=InnoDB CHARACTER SET=binary;

